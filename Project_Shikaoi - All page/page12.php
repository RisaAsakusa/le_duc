<?php $id="page4";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a class="border" href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="container">

	<?php
	//==============================================
	// .c-title2
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">女性部通信</span><br>
		JAshikaoi Woman’s Magazine
	</div>



	<?php
	//==============================================
	// c-tabs2 03 page 12
	//============================================== ?>
	<div class="l-content">
		<div class="c-tab3">
			<button class="tablinks4" onclick="openCity4(event, 'a2')" id="defaultOpen4">
			2017
			</button>
			<button class="tablinks4" onclick="openCity4(event, 'b2')">
			2016
			</button>
			<button class="tablinks4 tablinks4--margin" onclick="openCity4(event, 'c2')">
			2015
			</button>
		</div>
		<div id="a2" class="tabcontent4">
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav6_02.php'); ?>
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav6_02.php'); ?>
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav6_02.php'); ?>
		</div>

		<div id="b2" class="tabcontent4">
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav6_02.php'); ?>
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav6_02.php'); ?>
		</div>

		<div id="c2" class="tabcontent4">
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav6_02.php'); ?>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>