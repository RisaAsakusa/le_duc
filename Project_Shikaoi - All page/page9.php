<?php $id="page9";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a class="border" href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="container">

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">安心、安全 鹿追ブランド</span><br>
	Shikaoi Brand
</div>

<?php
//==============================================
// c-tabs1 01
//============================================== ?>
<div class="l-content">
	<div class="c-tab">
	  <button class="tablinks" onclick="openCity(event, 'tab1')" id="defaultOpen">鹿追ブランド - 畑作</button>
	  <button class="tablinks" onclick="openCity(event, 'tab2')">鹿追ブランド - 酪農</button>
	  <button class="tablinks tablinks--margin" onclick="openCity(event, 'tab3')">鹿追ブランド - 畜産</button>
	</div>
<?php
//==============================================
// c-tabs1 01 content1
//============================================== ?>
<div id="tab1" class="tabcontent">

<?php
//==============================================
// c-text3
//============================================== ?>
<div class="l-content">
	<div class="c-text3">
		<div class="c-text3__text1">
			<p>鹿追では、良い畑（土）を作り、生産が安定した高品質で美味しい作物を生産するために、 酪農家が多いことを活用して、酪農家の畑と交換して長期の輪作（※）を行うこ<br>
			とにより、 土が健全で、病気が少なくなり農薬の使用を減らしています。家畜から出される糞尿を堆肥化して、 有機物の多く含んだ畑を作って余分な化学肥料を使わない<br>
			ように進めています。（※）輪作とは：同一の畑に、数種の作物を年ごとに順に栽培して病害を避けること。</p>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">小麦</span><br>
	Wheat
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				鹿追町で生産される小麦は、冷涼な気候と有機物の多い圃場で時間を掛けて育った、
				良質で麺適正に優れた小麦です。品種は「きたほなみ」で、主にうどんに利用されて
				います。24年からはパン・中華めん用品種「ゆめちから」も生産し、国産小麦の供
				給拡大を進めています。
			</div>
		</div>
	</div>
</div>


<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">豆類</span><br>
	Beans
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				鹿追の豆は、豆の生育の気象条件に合い有機物の多い畑で育った十勝豆王国の中で
				も優れた品質の品物で、 土壌や気候条件を背景に化学肥料や農薬を極力減らした製
				品を提供しております。 また、減農薬・減化学肥料の特別栽培小豆を一部生産者が
				取組みホクレンを通して販売しております。
			</div>
		</div>
	</div>
</div>


<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">馬鈴薯</span><br>
	Potato
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				馬鈴薯が美味しく育つ条件である冷涼で寒暖の差がでんぷんを多く含んだ甘い馬鈴
				薯を作っています。 また、品質のよい馬鈴薯を作るために、徹底した管理を行って
				おります。 品種は、ポテトチップス・ポテトサラダ・デンプンなど用途に応じた多
				くの品種を、「士幌ブランド」で提供しております。
			</div>
		</div>
	</div>
</div>


<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">てん菜</span><br>
	Sugar Beat
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				てんさいは、砂糖大根とも呼ばれる砂糖の原料として北海道のみで栽培されている
				作物です。 鹿追町でのてんさいの生産は、肥沃な土壌を背景に化学肥料を最低限に
				抑えた栽培で、 良質な砂糖の原料をホクレン清水製糖工場へ供給しております。 
				ホクレン清水製糖工場では、ホクレンマークのグラニュ糖や上白糖の他、 オリゴ糖
				を含む「てんさい糖」など消費者ニーズに対応した商品の製造を行っております。
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">蕎麦</span><br>
	Soba
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				鹿追町の蕎麦は、風味や味を最高に引き出せる風土を持った地域で、 栽培面積はあ
				まり多くありませんが「鹿追蕎麦」として古くから全国的に評価が高いブランドで
				す。 平成４年にはそれらが評価され、全国そば生産優良経営で中瓜幕の生産集団が
				受賞しています。
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">坊ちゃん南瓜</span><br>
	Pumpkin
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				南瓜の中でも小玉で美味しい坊ちゃん南瓜をグランドマーク（土と接触する部分が
				黄変する事）が付かないように、トンネルで空中栽培しています。 成熟した南瓜を
				1つ1つ丁寧に収穫して、乾燥後に選別・箱詰めを行います。<br>
				（出荷時期9〜11月）
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">十勝高原キャベツ</span><br>
	Cabbage
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				『しかりべつ高原野菜出荷組合』を中心に、消費者ニーズに対応した安心・安全な
				キャベツを生産しています。 化学合成肥料を低減し、輸送体系（中5年以上）を守
				り、堆肥を3ｔ/10a以上使用して土壌分析により施肥を行い、徹底した 土作りによ
				り美味しいキャベツを生産しています。北海道のクリーン農産物基準である、
				『YES!clean』登録産地です。
			</div>
			<div class="l-flame3__text1">
				『 しかりべつ高原野菜出荷組合（ＪＡ鹿追町） 』 の取組み
				・1991(Ｈ3)年に設立して以来キャベツ栽培で、消費者ニーズに対応する『クリーン農業』を目
				指して推進しています。
			</div>
			<div class="l-flame3__text1">
				・ 化学合成肥料の低減を目指し、輪作体系（中５年以上）の維持・堆肥３t/10a以上の使用・高
				畝栽培や土壌分析による施肥等、 徹底した土作りを実施しています。※町内の1/2を占める酪農
				家から出る堆肥(糞尿)を利用し地力維持・増進を図ると共に、環境リサイクルを可能としていま
				す （畑作農家の小麦稈との交換、双方の処理に伴う循環）。
			</div>

			<div class="l-flame3__text1">
				・ 農薬使用量においても、病害虫発生の予察を効果的に行い、 病害虫の防除のみ最低限の使用
				量に抑えた栽培をしています。
			</div>

			<div class="l-flame3__text1">
				・ 食の安心、安全が消費者に強く求められる様になった現在に至っては、 上述の背景から北海
				道の推進するクリーン農産物基準である「YES！Clean」登録産地（キャベツでは全道に11箇所）
				として位置付けられています。<br>
				（出荷時期7月〜10月）
			</div>


		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">アスパラ</span><br>
	Green Asparagus
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				有機物をふんだんに投入した圃場で栽培され、寒暖差が激しい事から非常に甘くみ
				ずみずしいアスパラが生産されます。 贈答用にも出荷しており。北海道の旬の味を
				お届けしていますので、ご利用をお願いいたします。グリーンアスパラもキャベツ
				と同様に 安心・安全な『YES!clean』登録産地です。　注文先 ＪＡ鹿追町野菜セン
				ター TEL: 0156-66-1870<br>
				（出荷時期5〜6月）
			</div>
		</div>
	</div>
</div>
</div>

<?php
//==============================================
// c-tabs1 01 content2
//============================================== ?>
<div id="tab2" class="tabcontent">

<?php
//==============================================
// c-text3
//============================================== ?>
<div class="l-content">
	<div class="c-text3">
		<div class="c-text3__text1">
			<h2>十勝トップクラスの生産量</h2>
			<p>JA鹿追町の生産乳量は年間約10万トンで十勝エリアでは トップクラス を誇ります。</p>
		</div>
		<div class="c-text3__text1">
			<h2>鹿追町の酪農家は良質で健康な牛乳の生産に努めています。</h2>
			<p>「北海道のおいしさを、まっすぐ。」で有名なよつ葉乳業(株)の原料乳主産地で、道外の共同購入団体、生協（店頭販売は除く）向けの生産者指定牛乳の原料乳も鹿追町で
			生産しています。
			</p>
		</div>
		<div class="c-text3__text1">
			<h2>飼養管理</h2>
			<p>毎月、乳牛検定によりデータ集積をおこない、牛の健康状態や、牛乳の品質の向上、安定化を図るための取り組みをおこなっています。
			</p>
		</div>
	</div>
</div>



<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">牛乳</span><br>
	Milk
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				<h2>生産者指定牛乳(道外の共同購入団体での取扱い</h2>
				生産者指定牛乳とは、よつ葉乳業(株)より 『生産者の顔の見える牛乳』としてNON−
				GMO(非遺伝子組み換え) 飼料を使用した鹿追町の酪農家９戸を含む十勝管内５JAの
				20戸だけで生産する生乳を使用した牛乳です。現在、『よつ葉ノンホモ牛乳』 『北
				海道HTSTよつ葉牛乳』 『北海道よつ葉低脂肪牛乳』の３種類があり、それぞれのパ
				ッケージに生産者の顔写真が５戸ずつ掲載されています。
			</div>
		</div>
	</div>
</div>
</div>
<?php
//==============================================
// c-tabs1 01 content2
//============================================== ?>
<div id="tab3" class="tabcontent">
<?php
//==============================================
// c-text3
//============================================== ?>
<div class="l-content">
	<div class="c-text3">
		<div class="c-text3__text1">
			<p>鹿追では、良い畑（土）を作り、生産が安定した高品質で美味しい作物を生産するために、 酪農家が多いことを活用して、酪農家の畑と交換して長期の輪作（※）を行うこ
			とにより、 土が健全で、病気が少なくなり農薬の使用を減らしています。家畜から出される糞尿を堆肥化して、 有機物の多く含んだ畑を作って余分な化学肥料を使わない<br>
			ように進めています。（※）輪作とは：同一の畑に、数種の作物を年ごとに順に栽培して病害を避けること。</p>
		</div>
	</div>
</div>



<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">鹿追牛</span><br>
	Shikaoi Beef
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				<h2>鹿追生まれ・鹿追育ちの町内完結型</h2>
				鹿追町内で生産される乳雄仔牛（交雑種を含む）のみを「一貫肥育」しており、年
				間約4,800頭を出荷しています。 また履歴については「牛トレーサビリティ制度」
				の施行以前からすべての牛が明確で「鹿追生まれ・鹿追育ちの町内完結型」生産体
				系を確立しています。 
			</div>
		</div>
	</div>
</div>


<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">鹿追豚</span><br>
	Shikaoi Pork
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				<h2>鹿追生まれ・鹿追育ちの町内完結型</h2>
				健康な豚を育てるために飼料の原材料や配合にこだわり、豚に極力ストレスを与え
				ない飼育をおこなっています。 鹿追産の豚は弾力性に富んだ肉質に旨みのある脂肪
				部分が特徴です。 徹底的な衛生管理のもと安心して食べて頂ける豚肉として年間約
				9,000頭を出荷しています。 
			</div>
		</div>
	</div>
</div>



<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">純卵</span><br>
	Premium Egg 
</div>

<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				<h2>純天然の混合飼料 『地養素』 </h2>
				鹿追の純卵は、純天然の混合飼料 『地養素』 を与えた鶏から産まれた卵です。 特
				徴は卵黄が固く甘味が強く、生臭くないので生のまま食べるのに最適です。 卵黄の
				コレステロールが普通卵より5〜10%程少なく、アルカリ化されているので、腐りに
				くく、保存がききます。 鹿追の純卵は、無洗浄で無選卵です。また卵は産まれた時
				から殻の表面に外菌に対する抵抗力をもっています。 純卵は洗浄していませんので
				その抵抗力は高く、新鮮・長持ちです。
			</div>
			<div class="l-flame3__text1">
				<h2>地養素とは…</h2>
				畜産関係で広く利用されている、混合飼料"グローリッチ"（木酢精溜液、ゼオライト）の良さをそ
				のまま生かし、 更に生産物の「味と質の向上」を狙って、味質改善用天然物（海藻、ヨモギ粉末）
				を加えた、銘柄品作り主体の混合飼料のことです。
			</div>
		</div>
	</div>
</div>
</div>
</div>





</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>