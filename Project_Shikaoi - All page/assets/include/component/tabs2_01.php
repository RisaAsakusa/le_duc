<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">フォトギャラリー</span><br>
	Photo Gallery
</div>





<?php
//==============================================
// c-tabs2 01
//============================================== ?>
<div class="l-content">
	<div class="c-tab2">
		<button class="tablinks2" onclick="openCity2(event, 'a')" id="defaultOpen2">
		畑作フォトギャラリー
		</button>
		<button class="tablinks2" onclick="openCity2(event, 'b')">
		酪農フォトギャラリー
		</button>
		<button class="tablinks2 tablinks2--margin" onclick="openCity2(event, 'c')">
		畜産フォトギャラリー
		</button>
	</div>

	<div id="a" class="tabcontent2">
	<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav5_01.php'); ?>
	

<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>



<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>





	</div>




	<div id="b" class="tabcontent2">
<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/01.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/01_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/02.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/02_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/03.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/03_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/04.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/04_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>


<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>



<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>


















	</div>




	<div id="c" class="tabcontent2">

<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/01.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/thumb/01_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/02.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/thumb/02_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/03.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/thumb/03_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>


	</div>


</div>
