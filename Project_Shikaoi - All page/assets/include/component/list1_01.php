<?php
//==============================================
// c-list1
//============================================== ?>
<section class="option2">
	<div class="l-content">

	<div class="c-list1">
		<div class="title">
			<h3 class="u-red">NEWS & TOPICS</h3>
			新着情報
			<span class="u-color1">JA鹿追町からのお知らせ、トピックスをお届けいたします。</span>
		</div>
		<div class="c-line">
			<span class="u-size">
				2017.12.20
			</span>
			<span class="u-bgcolor">
				お知らせ
			</span>
			お知らせのタイトルを表示。
		</div>
		<div class="c-line">
			<span class="u-size">
				2017.12.20
			</span>
			<span class="u-bgcolor u-bgcolor--bg2">
				トピックス
			</span>
			トピックスのタイトルを表示。
		</div>
		<div class="c-line">
			<span class="u-size">
				2017.12.20
			</span>
			<span class="u-bgcolor u-bgcolor--bg2">
				トピックス
			</span>
			トピックスのタイトルを表示。
		</div>
		<div class="c-line">
			<span class="u-size">
				2017.12.20
			</span>
			<span class="u-bgcolor u-bgcolor--bg2">
				トピックス
			</span>
			トピックスのタイトルを表示。
		</div>
		<div class="c-line">
			<span class="u-size">
				2017.12.20
			</span>
			<span class="u-bgcolor u-bgcolor--bg3">
				更新
			</span>
			更新した内容タイトルを表示。
		</div>
	</div>

	</div>
</section>