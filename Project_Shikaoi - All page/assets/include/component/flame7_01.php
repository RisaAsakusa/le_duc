<?php
//==============================================
// l-flame7 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__right">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__light">
			<div class="l-flame3__text1">
				ＪＡ事業としては、全国で最初に設立され、コントラ課に委託した作業の軽減によ り、労働強化することなく、酪農の規模拡大と生乳生産の拡大が実現出来ました。 現在の酪農家1戸当たりの乳牛飼養頭数は約150頭と他地域を大きく上回っています。
			</div>
			<div class="l-flame3__text1">
				自給飼料生産の拡大と品質および栄養価の向上ならびに自給飼料生産コストが低減し、 堆肥等の圃場還元面積と量が増加し、購入肥料代が減少しました。結果として、環境保全型農業の進展を促しています。
			</div>
			<div class="l-flame3__text1">
				このコントラ事業のおかげで、酪農は、「土づくり」「草づくり」を基本とし、 良質粗飼料の確保・給与、新技術導入、乳牛遺伝改良を促進し生産性向上を図っています。酪農経営の安定と労働緩和を図り生活にゆとりを持たせる重要な事業として、組合員の経営に定着化しています。
			</div>
		</div>
	</div>
</div>
