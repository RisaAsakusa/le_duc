<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/01.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/01_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/02.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/02_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/03.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/03_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/04.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/04_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>
