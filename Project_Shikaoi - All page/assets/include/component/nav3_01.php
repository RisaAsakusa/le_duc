<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">職場紹介</span><br>
	Workplace
</div>

<?php
//==============================================
// c-nav3 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav3">
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>購買部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>購買部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>購買部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1  c-nav3__box1--margin"">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>購買部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
	</div>
</div>
<?php
//==============================================
// c-nav3 02
//============================================== ?>
<div class="l-content">
	<div class="c-nav3">
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1  c-nav3__box1--margin"">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
	</div>
</div>
<?php
//==============================================
// c-nav3 03
//============================================== ?>
<div class="l-content">
	<div class="c-nav3">
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1  c-nav3__box1--margin"">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
	</div>
</div>
<?php
//==============================================
// c-nav3 04
//============================================== ?>
<div class="l-content">
	<div class="c-nav3">
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
		<div class="c-nav3__box1  c-nav3__box1--margin"">
			<div class="c-nav3__img">
				<img src="assets/img/page5/workplace01.jpg" alt="">
			</div>
			<div class="c-nav3__text">
				<h2>◯◯部</h2>
				テキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div>
	</div>
</div>
