<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">おすすめレシピ</span><br>
	Archive 
</div>



<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>和食</h2>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>




<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>洋食・中華</h2>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>



<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>デザート・ケーキ</h2>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>