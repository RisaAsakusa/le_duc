<?php
//==============================================
// c-entry1 01
//============================================== ?>
<div class="l-content">
<div class="c-entry1">
	<div class="c-entry1__img">
		<img src="assets/img/page1/acoop_banner.jpg" alt="">
	</div>
	<div class="c-entry1__text">
		Aコープ鹿追店
		<h2>国産野菜統一宣言！ 地産地消をおいしく応援中。</h2>
		<p>Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>
		安心で安全な食品をご提供いたします。</p>
	</div>
</div>
<?php
//==============================================
// c-entry2
//============================================== ?>
<div class="c-entry1">
	<div class="c-entry1__img">
		<img src="assets/img/page1/furusato_banner.jpg" alt="">
	</div>
	<div class="c-entry1__text">
		ふるさと納税返礼品［鹿追町］
		<h2>JA鹿追町のふるさとチョイス。</h2>
		<p>安心、安全な鹿追産の牛肉、豚肉、ハンバーグをご提供いたします。<br>
		是非ご利用ください。</p>
	</div>
</div>
<?php
//==============================================
// c-entry3
//============================================== ?>
<div class="c-entry1">
	<div class="c-entry1__img">
		<img src="assets/img/page1/recruit_banner.jpg" alt="">
	</div>
	<div class="c-entry1__text">
		農業求人情報 <span class="u-bgred">NEW</span>
		<h2>酪農スタッフ募集のお知らせ。</h2>
		<p>酪農スタッフを募集しています。<br>
		広大な十勝平野で私たちと一緒に働きませんか！</p>
	</div>
</div>
<?php
//==============================================
// c-entry4
//============================================== ?>
<div class="c-entry1">
	<div class="c-entry1__img">
		<img src="assets/img/page1/recipe_banner.jpg" alt="">
	</div>
	<div class="c-entry1__text">
		とっておきのレシピ <span class="u-bgred">NEW</span>
		<h2>野菜の大根おろしあえ vol.89</h2>
		<p>旬の野菜を大根おろしであえ特性タレをかけました。<br>
		ごはんのおかずにもビールのおつまににも良く合います。</p>
	</div>
</div>
<?php
//==============================================
// c-entry5
//============================================== ?>
<div class="c-entry1">
	<div class="c-entry1__img">
		<img src="assets/img/page1/jabook_banner.jpg" alt="">
	</div>
	<div class="c-entry1__text">
		JA通信しかおい
		<h2>JA通信11月号を公開しました。</h2>
		<p>農業ニュースや各種イベントなど楽しい話題をお届けいたします。<br>
		鹿追町内の皆さまへ毎月無料配布をおこなっています。</p>
	</div>
</div>
</div>

