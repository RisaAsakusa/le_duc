<?php $id="page6";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a class="border" href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="container">

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">中古農機・中古車情報</span><br>
	Used Machine
</div>

<?php
//==============================================
// .c-title3
//============================================== ?>
<div class="c-title1 c-title1--size2">
	中古トラクター<br>
	<span class="u-size2">XXX-15</span>
</div>

<?php
//==============================================
// l-flame4 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame4">
		<div class="l-flame4__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame4__right">
			<table>
				<tr>
					<td>メーカー名</td>
					<td>
						<span class="td1">三菱</span>
						<span class="td2">形式</span>
						<span class="td1">XXX-15</span>
					</td>
				</tr>
				<tr>
					<td>年式</td>
					<td>XXX-XXX</td>
				</tr>
				<tr>
					<td>本体価格</td>
					<td>1,000,000円（税別）</td>
				</tr>
				<tr>
					<td>整備状況</td>
					<td>整備済み</td>
				</tr>
				<tr>
					<td>付属品</td>
					<td>無し</td>
				</tr>
				<tr>
					<td>消耗品</td>
					<td>タイヤ8部山</td>
				</tr>
				<tr>
					<td>お問い合わせ</td>
					<td>担当 ◯◯◯まで。tel 0000-00-0000</td>
				</tr>
				<tr>
					<td>詳細説明</td>
					<td>説明テキスト。説明テキスト。説明テキスト。説明テキ
					スト。説明テキスト。説明テキスト。説明テキスト。説
					明テキスト。説明テキスト。説明テキスト。</td>
				</tr>
			</table>

		</div>
	</div>
</div>

<?php
//==============================================
// .c-title3
//============================================== ?>
<div class="c-title1 c-title1--size2">
	中古トラクター<br>
	<span class="u-size2">XXX-15</span>
</div>

<?php
//==============================================
// l-flame4 02
//============================================== ?>
<div class="l-content">
	<div class="l-flame4">
		<div class="l-flame4__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame4__right">
			<table>
				<tr>
					<td>メーカー名</td>
					<td>
						<span class="td1">三菱</span>
						<span class="td2">形式</span>
						<span class="td1">XXX-15</span>
					</td>
				</tr>
				<tr>
					<td>年式</td>
					<td>XXX-XXX</td>
				</tr>
				<tr>
					<td>本体価格</td>
					<td>1,000,000円（税別）</td>
				</tr>
				<tr>
					<td>整備状況</td>
					<td>整備済み</td>
				</tr>
				<tr>
					<td>付属品</td>
					<td>無し</td>
				</tr>
				<tr>
					<td>消耗品</td>
					<td>タイヤ8部山</td>
				</tr>
				<tr>
					<td>お問い合わせ</td>
					<td>担当 ◯◯◯まで。tel 0000-00-0000</td>
				</tr>
				<tr>
					<td>詳細説明</td>
					<td>説明テキスト。説明テキスト。説明テキスト。説明テキ
					スト。説明テキスト。説明テキスト。説明テキスト。説
					明テキスト。説明テキスト。説明テキスト。</td>
				</tr>
			</table>

		</div>
	</div>
</div>

<?php
//==============================================
// .c-title3
//============================================== ?>
<div class="c-title1 c-title1--size2">
	中古トラクター<br>
	<span class="u-size2">XXX-15</span>
</div>

<?php
//==============================================
// l-flame4 03
//============================================== ?>
<div class="l-content">
	<div class="l-flame4">
		<div class="l-flame4__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame4__right">
			<table>
				<tr>
					<td>メーカー名</td>
					<td>
						<span class="td1">三菱</span>
						<span class="td2">形式</span>
						<span class="td1">XXX-15</span>
					</td>
				</tr>
				<tr>
					<td>年式</td>
					<td>XXX-XXX</td>
				</tr>
				<tr>
					<td>本体価格</td>
					<td>1,000,000円（税別）</td>
				</tr>
				<tr>
					<td>整備状況</td>
					<td>整備済み</td>
				</tr>
				<tr>
					<td>付属品</td>
					<td>無し</td>
				</tr>
				<tr>
					<td>消耗品</td>
					<td>タイヤ8部山</td>
				</tr>
				<tr>
					<td>お問い合わせ</td>
					<td>担当 ◯◯◯まで。tel 0000-00-0000</td>
				</tr>
				<tr>
					<td>詳細説明</td>
					<td>説明テキスト。説明テキスト。説明テキスト。説明テキ
					スト。説明テキスト。説明テキスト。説明テキスト。説
					明テキスト。説明テキスト。説明テキスト。</td>
				</tr>
			</table>

		</div>
	</div>
</div>








</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>