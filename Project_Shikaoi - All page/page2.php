<?php $id="page2";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a class="border" href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>

<div class="container">
	<?php
	//==============================================
	// .c-title2 01
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">組合長からのご挨拶</span><br>
		感謝の思いを込めて
	</div>

	<?php
	//==============================================
	// l-flame1 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame1">
			<div class="l-flame1__left">
				<a class="example-image-link" href="assets/img/page2/kumiaicho.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page2/kohata.jpg" alt="image-1" /></a>
			</div>
			<div class="l-flame1__right">
				<div class="l-flame1__text1">
					　当ＪＡは、農村の民主化と社会的経済的地位の向上を目指して、昭和23年に設立を致しました。<br>
					思えばこの自然環境の美しい鹿追町も我々の先達が困苦欠乏に耐え忍び乍らも、未来に楽土を夢み開拓に汗、血を
					流したのであります。当時は一部の営利商人や金融家の手により流通機構が形成され、中には財産の全てを業者に
					依存せねばならない農家も少なくはなく、 これらの重圧から経営を守る組織として産業組合が設立され、農業協
					同組合へと引き継がれ幾多の試練を乗り越え今日に至りました。
				</div>
				<div class="l-flame1__text1">
					　今日では想いも及ばない事ではありますが、これらの教訓と歴史を礎に現在のＪＡが生き抜いてこられたことであ
					り、 農家組合員そして地域社会の中で生業させて頂いていることに対し深く感謝の念を申し上げます。<br>
					昨今の社会・経済の環境変 化が著しい中、当ＪＡが地域社会のリーダーとして理解され親しまれる様、 また、今
					後も巾広く貢献出来る様に事業を推めてまいりますので、皆様のご利用・ご協力を頂きたいと存ずる次第です。
				</div>
				<div class="l-flame1__text2">
					ＪＡ鹿追町　代表理事組合長　木幡浩喜
				</div>
			</div>
		</div>
	</div>

	<?php
	//==============================================
	// .c-title2 02
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">ＪＡ綱領</span><br>
		わたしたちＪＡのめざすもの
	</div>

	<?php
	//==============================================
	// c-img1 01
	//============================================== ?>
	<div class="l-content">
		<div class="c-img1">
			<img src="assets/img/page2/img1.png" alt="">
		</div>
	</div>

	<?php
	//==============================================
	// c-text1
	//============================================== ?>
	<div class="l-content">
		<div class="c-text1">
			<div class="c-text1__text1">
				　わたしたちＪＡの組合員・役職員は、協同組合運動の基本的な定義・価値・原則（自主、自立、参加、民主的運営、公正、連帯等）に基づき行動します。<br>
				 そして、地球的視野に立って環境変化を見通し、組織・事業・経営の革新を図ります。さらに、地域・全国・世界の協同組合の仲間と連携し、より民主的で公正な社会の実<br>
				現に努めます。このため、わたしたちは次のことを通じ、農業と地域社会に根ざした組織としての社会的役割を誠実に果たします。
			</div>
			<div class="c-text1__text1">
				わたしたちは、<br>
				一、地域の農業を振興し、わが国の食と緑と水を守ろう。<br>
				一、環境・文化・福祉への貢献を通じて、安心して暮らせる豊かな地域社会を築こう。<br>
				一、ＪＡへの積極的な参加と連帯によって、協同の成果を実現しよう。<br>
				一、自主・自立と民主的運営の基本に立ち、ＪＡを健全に経営し信頼を高めよう。<br>
				一、協同の理念を学び実践を通じて、共に生きがいを追求しよう。
			</div>
		</div>
	</div>

	<?php
	//==============================================
	// .c-title2 03
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">基本理念</span><br>
		当ＪＡは４つの基本理念の下、事業展開を行います。
	</div>

	<?php
	//==============================================
	// c-text1 02
	//============================================== ?>
	<div class="l-content">
		<div class="c-text1">
			<div class="c-text1__text1">
				　JAは、人々が連帯し助け合うことを意味する「相互扶助」の精神のもとに、組合員農家の農業経営と生活を守り、より良い地域社会を築くことを<br>
				目的につくられた協同組合です。JA鹿追町は、次の基本理念の下事業を展開します。
			</div>
		</div>
	</div>

	<?php
	//==============================================
	// c-text2
	//============================================== ?>
	<div class="l-content">
		<div class="c-text2">
			<div class="c-text2__text1">
				<h2>1. 真に農協らしい農協</h2>
				<p>組合員のための事業展開（組合員の経済・生活上必要な事業であっても決して農協のためであってはならない）</p>
			</div>
			<div class="c-text2__text1">
				<h2>2. 正確な情報を正しく提供（公開）出来る農協</h2>
				<p>農協の経営状況・財務等を正しく公開するとともに、農業情勢および営農上必要とする情報を正しく伝え、組合員が適正な判断が出来る様にします</p>
			</div>
			<div class="c-text2__text1">
				<h2>３. 組合員が結集出来る農協</h2>
				<p>組合員が必要とする農協。組合員の意見を積極的に聞くとともに動向を把握し事業展開を行う。理念だけではなく、他に負けない実利伴う購買・販売事業と組合員に応える利用事業及び農業支援システムの展開</p>
			</div>
			<div class="c-text2__text1">
				<h2>4. 地域住民に応える農協</h2>
				<p>農家組合員だけでなく、地域の住民に対し金融・共済事業、給油所、整備工場や生活店舗を含む事業により貢献する農協</p>
			</div>
		</div>
	</div>

		<?php
	//==============================================
	// .c-title2 04
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">組織</span><br>
		概況
	</div>

	<?php
	//==============================================
	// c-list2 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-list2">
			<div class="l-list2__left">
				<div class="l-list2__text">
					<div class="c-left">
						（設立）
					</div>
					<div class="c-right">
						昭和23年3月1日
					</div>
				</div>
				<div class="l-list2__text">
					<div class="c-left">
						（組合員）
					</div>
					<div class="c-right">
						正組合員 248名　法人 31名　准組合員 997名
					</div>
				</div>
				<div class="l-list2__text">
					<div class="c-left">
						（役員）
					</div>
					<div class="c-right">
						理事 14名　監事 5名<br>
						代表理事組合長 1名<br>
						専務理事 1名<br>
						常勤監事 1名<br>
						非常勤理事 1名<br>
						非常勤監事 4名<br>
						計 19名<br>
					</div>
				</div>
				<div class="l-list2__text">
					<div class="c-left">
						（設立）
					</div>
					<div class="c-right">
						一般職員 76名（男）・33名（女）計 109名<br>
						工場技術員 17名（男）計 17名<br>
						計 93名（男）・33名（女）<br>
					</div>
				</div>
			</div>
			<div class="l-list2__right">
				<div class="l-list2__text">
					<div class="c-left">
						（出資金）
					</div>
					<div class="c-right">
						総口数 272,924口<br>
						総額 1,264,620千円<br>
						一口金額 5,000円<br>
						最高限度 10,000口<br>
					</div>
				</div>
				<div class="l-list2__text">
					<div class="c-left">
						（運営）
					</div>
					<div class="c-right">
						総会 − 毎年5月開催（通常<br>
						理事会 − 毎月1回（定例）<br>
						監査 − 4半期毎（定例）<br>
					</div>
				</div>
				<div class="l-list2__text">
					<div class="c-left">
						（外かく組織）
					</div>
					<div class="c-right">
						◇ 酪農事業推進部会 − 16名<br>
						◇ 農産事業推進部会 − 16名<br>
						◇ 畜産事業推進部会 − 4名<br>
						◇ 作業受委託事業推進部会 − 16名<br>
						◇ 女性の会 − 36名<br>
						◇ JA青年部 − 83名<br>
						◇ JA女性部 − 141名<br>
						（フレッシュミズ会員54名・喜楽会員50名）<br>
						◇ 熟年会 − 29名<br>
						◇ 年金友の会 − 901名<br>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php
	//==============================================
	// .c-title2 05
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">組織機構図</span><br>
		フローチャート
	</div>


	<?php
	//==============================================
	// c-img1 02
	//============================================== ?>
	<div class="l-content">
		<div class="c-img1">
			<img src="assets/img/page2/img3.png" alt="">
		</div>
	</div>


	<?php
	//==============================================
	// .c-title2 06
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">職員募集のお知らせ</span><br>
		Recruitment
	</div>

	<?php
	//==============================================
	// l-flame2 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame2">
			<div class="l-flame2__left">
				JA鹿追町では職員の募集を行っています。<br>
				2018年4月採用 1〜3名を予定しています。<br>
				<a href="#"><span class="c-more">募集要項はこちら</span></a>
			</div>
			<div class="l-flame2__right">
				<img src="assets/img/page2/img2.png" alt="">
			</div>
		</div>
	</div>

</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>