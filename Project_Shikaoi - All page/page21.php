<?php $id="page21";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="container">
	<?php
	//==============================================
	// .c-title2
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">アクセスマップ </span><br>
		Access Map
	</div>


	<?php
	//==============================================
	// l-flame11 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame11">
			<div class="l-flame11__left">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.454883805347!2d106.70261311489182!3d10.776430292321457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f466ec1f455%3A0x6b9c06eab4b68f9d!2sC%C3%B4ng+Ty+TNHH+Allgrow+Labo!5e0!3m2!1svi!2s!4v1531983151089" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="l-flame11_right">
				<div class="l-flame11__text1">
					<h2>ＪＲ石勝線　新得駅より</h2>
					ＪＲを利用して札幌から約２時間で最寄のＪＲ新得駅に着きます。<br>
					拓殖バス・タクシー等で約２０分の距離です。
				</div>
				<div class="l-flame11__text1">
					<h2>帯広駅より</h2>
					鹿追・然別湖行き拓殖バスに乗り、約１時間の鹿追営業所前で下車します。<br>
					道路向かいが当ＪＡです。
				</div>
				<div class="l-flame11__text1">
					<h2>帯広空港より</h2>
					航空機で帯広空港に到着後、バスに乗り約５０分の帯広駅前で鹿追・然別湖行き<br>
					拓殖バスに乗り換えます。またはレンタカーをご利用ください。
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>