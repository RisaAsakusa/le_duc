<?php $id="page7";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a class="border" href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="container">

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">農業求人</span><br>
	Agricultural Recruitment
</div>

<?php
//==============================================
// c-text3
//============================================== ?>
<div class="l-content">
	<div class="c-text3">
		<div class="c-text3__text1">
			<h2>ＪＡ鹿追町無料職業紹介事業所</h2>
			<p>ＪＡ鹿追町無料職業紹介事業所では鹿追町の農業求人をご紹介しております。ご希望の求人情報がございましたら下記までお問い合わせ下さい。<br>
			※期間経過のため、募集が終了していることがありますので、ご了承ください。</p>
		</div>
		<div class="c-text3__text1">
			<h2>【お問い合わせ先】</h2>
			<p>ＪＡ鹿追町営農部企画振興課<br>
			tel 0156-66-3401　fax 0156-66-3194<br>
			担当　寺島 悟</p>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-text2
//============================================== ?>
<div class="l-content">
	<div class="c-text4">
		<div class="c-text4__text1">
			<h2>◯◯牧場</h2>
			<p>牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を
			掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。</p>
		</div>
	</div>
</div>

<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn1">
		<div class="c-btn1__btn1">
			<a href="index.php">ホームページ</a>
		</div>
		<div class="c-btn1__btn1">
			<a href="#">ブログ</a>
		</div>
		<div class="c-btn1__btn2">
			<a href="#"><img src="assets/img/page7/iconfb.png" alt=""></a>
		</div>
	</div>
</div>

<?php
//==============================================
// l-flame5 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame5">
		<div class="l-flame5__left">
			<img src="assets/img/page7/img1.png" alt="">
		</div>
		<div class="l-flame5__right">
			<table>
				<tr>
					<td>職種</td>
					<td>
						テキストテキスト
					</td>
				</tr>
				<tr>
					<td>資格</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務先</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務時間</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>休日</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>給与</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>待遇</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>備考</td>
					<td>テキストテキスト</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav4_01.php'); ?>

<?php
//==============================================
// c-text2
//============================================== ?>
<div class="l-content">
	<div class="c-text4">
		<div class="c-text4__text1">
			<h2>◯◯牧場</h2>
			<p>牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を
			掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。</p>
		</div>
	</div>
</div>

<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn1">
		<div class="c-btn1__btn1">
			<a href="index.php">ホームページ</a>
		</div>
		<div class="c-btn1__btn1">
			<a href="#">ブログ</a>
		</div>
		<div class="c-btn1__btn2">
			<a href="#"><img src="assets/img/page7/iconfb.png" alt=""></a>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame5 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame5">
		<div class="l-flame5__left">
			<img src="assets/img/page7/img1.png" alt="">
		</div>
		<div class="l-flame5__right">
			<table>
				<tr>
					<td>職種</td>
					<td>
						テキストテキスト
					</td>
				</tr>
				<tr>
					<td>資格</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務先</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務時間</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>休日</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>給与</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>待遇</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>備考</td>
					<td>テキストテキスト</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav4_01.php'); ?>



<?php
//==============================================
// c-text2
//============================================== ?>
<div class="l-content">
	<div class="c-text4">
		<div class="c-text4__text1">
			<h2>◯◯牧場</h2>
			<p>牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を
			掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。</p>
		</div>
	</div>
</div>



<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn1">
		<div class="c-btn1__btn1">
			<a href="index.php">ホームページ</a>
		</div>
		<div class="c-btn1__btn1">
			<a href="#">ブログ</a>
		</div>
		<div class="c-btn1__btn2">
			<a href="#"><img src="assets/img/page7/iconfb.png" alt=""></a>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame5 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame5">
		<div class="l-flame5__left">
			<img src="assets/img/page7/img1.png" alt="">
		</div>
		<div class="l-flame5__right">
			<table>
				<tr>
					<td>職種</td>
					<td>
						テキストテキスト
					</td>
				</tr>
				<tr>
					<td>資格</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務先</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務時間</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>休日</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>給与</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>待遇</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>備考</td>
					<td>テキストテキスト</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav4_01.php'); ?>



<?php
//==============================================
// c-text2
//============================================== ?>
<div class="l-content">
	<div class="c-text4">
		<div class="c-text4__text1">
			<h2>◯◯牧場</h2>
			<p>牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を
			掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。</p>
		</div>
	</div>
</div>



<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn1">
		<div class="c-btn1__btn1">
			<a href="index.php">ホームページ</a>
		</div>
		<div class="c-btn1__btn1">
			<a href="#">ブログ</a>
		</div>
		<div class="c-btn1__btn2">
			<a href="#"><img src="assets/img/page7/iconfb.png" alt=""></a>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame5 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame5">
		<div class="l-flame5__left">
			<img src="assets/img/page7/img1.png" alt="">
		</div>
		<div class="l-flame5__right">
			<table>
				<tr>
					<td>職種</td>
					<td>
						テキストテキスト
					</td>
				</tr>
				<tr>
					<td>資格</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務先</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務時間</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>休日</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>給与</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>待遇</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>備考</td>
					<td>テキストテキスト</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav4_01.php'); ?>



<?php
//==============================================
// c-text2
//============================================== ?>
<div class="l-content">
	<div class="c-text4">
		<div class="c-text4__text1">
			<h2>◯◯牧場</h2>
			<p>牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を
			掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。牧場の紹介文を掲載。</p>
		</div>
	</div>
</div>



<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn1">
		<div class="c-btn1__btn1">
			<a href="index.php">ホームページ</a>
		</div>
		<div class="c-btn1__btn1">
			<a href="#">ブログ</a>
		</div>
		<div class="c-btn1__btn2">
			<a href="#"><img src="assets/img/page7/iconfb.png" alt=""></a>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame5 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame5">
		<div class="l-flame5__left">
			<img src="assets/img/page7/img1.png" alt="">
		</div>
		<div class="l-flame5__right">
			<table>
				<tr>
					<td>職種</td>
					<td>
						テキストテキスト
					</td>
				</tr>
				<tr>
					<td>資格</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務先</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>勤務時間</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>休日</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>給与</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>待遇</td>
					<td>テキストテキスト</td>
				</tr>
				<tr>
					<td>備考</td>
					<td>テキストテキスト</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/nav4_01.php'); ?>







</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>