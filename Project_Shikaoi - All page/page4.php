<?php $id="page4";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a class="border" href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="container">
	<?php
	//==============================================
	// .c-title2 01
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">青年部</span><br>
		Youth Community 
	</div>

	<?php
	//==============================================
	// l-flame3 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame3">
			<div class="l-flame3__left">
				<div class="c-slider1">
					<div class="flexslider1">
						<ul class="slides">
					     	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="l-flame3__right">
				<div class="l-flame3__text2">
					<h2>タイトル タイトル</h2>
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					 
				</div>
				<div class="l-flame3__text2">
					<h2>タイトル タイトル</h2>
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				</div>
				<div class="l-flame3__btn1">
					<a href="page13.php">
						<div class="c-btn1">
							青年部活動日誌はこちら
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<?php
	//==============================================
	// .c-title2 02
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">女性部</span><br>
		Ladies Community 
	</div>


	<?php
	//==============================================
	// l-flame3 02
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame3">
			<div class="l-flame3__left">
				<div class="c-slider1">
					<div class="flexslider1">
						<ul class="slides">
					     	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="l-flame3__right">
				<div class="l-flame3__text2">
					<h2>タイトル タイトル</h2>
					女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
					女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
					女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。 
				</div>
				<div class="l-flame3__text2">
					<h2>タイトル タイトル</h2>
					女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
					女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
					女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。 
				</div>
				<div class="l-flame3__btn1">
					<a href="page12.php">
						<div class="c-btn1">
							女性部活動日誌はこちら
						</div>
					</a>
					<a href="page15.php">
						<div class="c-btn1 c-btn1--color1">
							とっておきのレシピはこちら
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<?php
	//==============================================
	// .c-title2 03
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">熟年会</span><br>
		Senior Community
	</div>


	<?php
	//==============================================
	// l-flame3 03
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame3">
			<div class="l-flame3__left">
				<div class="c-slider1">
					<div class="flexslider1">
						<ul class="slides">
					     	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="l-flame3__right">
				<div class="l-flame3__text2">
					<h2>タイトル タイトル</h2>
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたし
					ます。青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願い
					いたします。 
				</div>
				<div class="l-flame3__text2">
					<h2>タイトル タイトル</h2>
					青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたし
					ます。青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願い
					いたします。青年部の紹介テキストをお願いいたします。青年部の紹介テキストを
					お願いいたします。青年部の紹介テキストをお願いいたします。青年部の紹介テキ
					ストをお願いいたします。
				</div>
				<div class="l-flame3__btn1">
					<a href="page14.php">
						<div class="c-btn1">
							熟年会活動日誌はこちら
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>