<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">とっておきのレシピ</span><br>
	My favorite Recipe 
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/title4_01.php'); ?>
<?php

//==============================================
// l-flame8 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame8">
		<div class="l-flame8__left">
			<img src="assets/img/page15/recipe_84.jpg" alt="">
			<div class="l-flame8__color">
				ワンポイント
			</div>
			<div class="l-flame8__text">
				料理のコツやワンポイントアドバイスを掲載。料理のコツやワンポイントアドバ
				イスを掲載。料理のコツやワンポイントアドバイスを掲載。料理のコツやワンポ
				イントアドバイスを掲載。料理のコツやワンポイントアドバイスを掲載。
			</div>
		</div>
		<div class="l-flame8__right">
			<table>
				<tr>
					<th>材料・調味料</th>
					<th>分量</th>
					<th>備考</th>
				</tr>
				<tr>
					<td>カボチャ</td>
					<td>1個</td>
					<td></td>
				</tr>
				<tr>
					<td>バター</td>
					<td>小さじ1</td>
					<td></td>
				</tr>
				<tr>
					<td>塩コショウ</td>
					<td>少々</td>
					<td></td>
				</tr>
				<tr>
					<td>カレーパウダー</td>
					<td>大さじ3</td>
					<td></td>
				</tr>
				<tr>
					<td>ピザ用チーズ</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td>（衣）卵</td>
					<td>1個</td>
					<td></td>
				</tr>
				<tr>
					<td>水</td>
					<td>50cc</td>
					<td></td>
				</tr>
				<tr>
					<td>薄力粉</td>
					<td>大さじ4</td>
					<td></td>
				</tr>
				<tr>
					<td>パン粉</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td>黒ゴマ</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td>揚げ油</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</div>