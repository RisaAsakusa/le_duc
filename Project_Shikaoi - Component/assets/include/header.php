<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="">ホーム</a></li>
				<li><a href="">JA鹿追町について</a></li>
				<li><a href="">鹿追町の農業</a></li>
				<li><a href="">青年部・女性部・熟年会</a></li>
				<li><a href="">職場紹介</a></li>
				<li><a href="">組合員情報</a></li>
				<li><a href="">農業求人</a></li>
				<li><a href="">新着情報</a></li>
			</ul>
		</nav>
	</div>
</header>
