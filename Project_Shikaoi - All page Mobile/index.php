<?php $id="index";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a class="border" href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>



<div class="container">
	<?php
	/*==============================================
	slider
	================================================*/ ?>
	<section class="p-index">
		<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/slider1_01.php'); ?>
	</section>

	<?php
	//==============================================
	// c-list1
	//============================================== ?>
	<section class="p-index1">
		<div class="l-content">
			<div class="c-list1">
				<div class="title">
					<h3 class="u-red">NEWS & TOPICS</h3>
					新着情報
					<span class="u-color1">JA鹿追町からのお知らせ、トピックスをお届けいたします。</span>
				</div>
				<div class="c-line">
					<span class="u-size">
						2017.12.20
					</span>
					<span class="u-bgcolor">
						お知らせ
					</span>
					お知らせのタイトルを表示。
				</div>
				<div class="c-line">
					<span class="u-size">
						2017.12.20
					</span>
					<span class="u-bgcolor u-bgcolor--bg2">
						トピックス
					</span>
					トピックスのタイトルを表示。
				</div>
				<div class="c-line">
					<span class="u-size">
						2017.12.20
					</span>
					<span class="u-bgcolor u-bgcolor--bg2">
						トピックス
					</span>
					トピックスのタイトルを表示。
				</div>
				<div class="c-line">
					<span class="u-size">
						2017.12.20
					</span>
					<span class="u-bgcolor u-bgcolor--bg2">
						トピックス
					</span>
					トピックスのタイトルを表示。
				</div>
				<div class="c-line">
					<span class="u-size">
						2017.12.20
					</span>
					<span class="u-bgcolor u-bgcolor--bg3">
						更新
					</span>
					更新した内容タイトルを表示。
				</div>
			</div>
		</div>
	</section>

	<?php
	//==============================================
	// .c-title1 01
	//============================================== ?>
	<div class="c-title1">
		<span class="u-size2">安心、安全  鹿追ブランド</span><br>
		Shikaoi Brand
	</div>

	<?php
	//==============================================
	// c-nav1 01
	//============================================== ?>
	<section class="p-index2">
		<div class="l-content">
			<div class="c-nav1">
				<div class="c-nav1__box1">
					<div class="c-nav1__img">
						<img src="assets/img/page1/shikaoi_brand01.jpg" alt="">
					</div>
					<div class="c-nav1__text">
						<h2>肥沃な大地で生産する安全な農作物</h2>
						テキストテキストテキストテキストテキストテキストテキスト
						テキストテキストテキストテキストテキスト
					</div>
				</div>
				<div class="c-nav1__box1 c-nav1__box1--margin">
					<div class="c-nav1__img">
						<img src="assets/img/page1/shikaoi_brand02.jpg" alt="">
					</div>
					<div class="c-nav1__text">
						<h2>北海道を代表する高品質の牛乳</h2>
						テキストテキストテキストテキストテキストテキストテキスト
						テキストテキストテキストテキストテキスト
					</div>
				</div>
				<div class="c-nav1__box1">
					<div class="c-nav1__img">
						<img src="assets/img/page1/shikaoi_brand03.jpg" alt="">
					</div>
					<div class="c-nav1__text">
						<h2>信頼の「鹿追ブランド」牛肉・豚肉</h2>
						テキストテキストテキストテキストテキストテキストテキスト
						テキストテキストテキストテキストテキスト
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	//==============================================
	// .c-title1 02
	//============================================== ?>
	<div class="c-title1">
		<span class="u-size2">青年部・女性部・熟年会</span><br>
		Shikaoi Community
	</div>

	<?php
	//==============================================
	// c-nav1 02
	//============================================== ?>
	<section class="p-index3">
		<div class="l-content">
			<div class="c-nav1">
				<div class="c-nav1__box1">
					<div class="c-nav1__img">
						<img src="assets/img/page1/shikaoi_brand04.jpg" alt="">
					</div>
					<div class="c-nav1__text">
						<h2>青年部</h2>
						テキストテキストテキストテキストテキストテキストテキスト
						テキストテキストテキストテキストテキスト
					</div>
				</div>
				<div class="c-nav1__box1 c-nav1__box1--margin">
					<div class="c-nav1__img">
						<img src="assets/img/page1/shikaoi_brand05.jpg" alt="">
					</div>
					<div class="c-nav1__text">
						<h2>女性部</h2>
						テキストテキストテキストテキストテキストテキストテキスト
						テキストテキストテキストテキストテキスト
					</div>
				</div>
				<div class="c-nav1__box1">
					<div class="c-nav1__img">
						<img src="assets/img/page1/shikaoi_brand06.jpg" alt="">
					</div>
					<div class="c-nav1__text">
						<h2>熟年会</h2>
						テキストテキストテキストテキストテキストテキストテキスト
						テキストテキストテキストテキストテキスト
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	//==============================================
	// .c-title1 03
	//============================================== ?>
	<div class="c-title1">
		<span class="u-size2">フォトギャラリー</span><br>
		Photo Gallery
	</div>

	<?php
	//==============================================
	// c-nav2 01
	//============================================== ?>
	<section class="p-index4">
		<div class="c-nav2">
			<img src="assets/img/page1/photogallery_banner01.jpg" alt="">
		</div>
	</section>
	<?php
	//==============================================
	// c-nav2 02
	//============================================== ?>
	<section class="p-index4">
		<div class="c-nav2">
			<img src="assets/img/page1/photogallery_banner02.jpg" alt="">
		</div>
	</section>
	<?php
	//==============================================
	// c-nav2 03
	//============================================== ?>
	<section class="p-index4">
		<div class="c-nav2">
			<img src="assets/img/page1/photogallery_banner03.jpg" alt="">
		</div>
	</section>

	<?php
	//==============================================
	// .c-title1 03
	//============================================== ?>
	<div class="c-title1">
		<span class="u-size2">インフォメーション</span><br>
		Information
	</div>

	<?php
	//==============================================
	// c-entry1 01
	//============================================== ?>
	<section class="p-index5">
		<div class="l-content">
		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/acoop_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				Aコープ鹿追店
				<h2>国産野菜統一宣言！ 地産地消をおいしく応援中。</h2>
				<p>Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>
				安心で安全な食品をご提供いたします。</p>
			</div>
		</div>
		<?php
		//==============================================
		// c-entry2
		//============================================== ?>
		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/furusato_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				ふるさと納税返礼品［鹿追町］
				<h2>JA鹿追町のふるさとチョイス。</h2>
				<p>安心、安全な鹿追産の牛肉、豚肉、ハンバーグをご提供いたします。<br>
				是非ご利用ください。</p>
			</div>
		</div>
		<?php
		//==============================================
		// c-entry3
		//============================================== ?>
		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/recruit_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				農業求人情報 <span class="u-bgred">NEW</span>
				<h2>酪農スタッフ募集のお知らせ。</h2>
				<p>酪農スタッフを募集しています。<br>
				広大な十勝平野で私たちと一緒に働きませんか！</p>
			</div>
		</div>
		<?php
		//==============================================
		// c-entry4
		//============================================== ?>
		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/recipe_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				とっておきのレシピ <span class="u-bgred">NEW</span>
				<h2>野菜の大根おろしあえ vol.89</h2>
				<p>旬の野菜を大根おろしであえ特性タレをかけました。<br>
				ごはんのおかずにもビールのおつまににも良く合います。</p>
			</div>
		</div>
		<?php
		//==============================================
		// c-entry5
		//============================================== ?>
		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/jabook_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				JA通信しかおい
				<h2>JA通信11月号を公開しました。</h2>
				<p>農業ニュースや各種イベントなど楽しい話題をお届けいたします。<br>
				鹿追町内の皆さまへ毎月無料配布をおこなっています。</p>
			</div>
		</div>
		</div>
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
