<?php $id="page20";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a class="border" href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">
	<?php
		//==============================================
		// .c-title2
		//============================================== ?>
		<div class="c-title1 c-title1--size">
			<span class="u-size2">リンク  </span><br>
			Links
		</div>

<?php
//==============================================
// l-flame10 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame10">
		<div class="l-flame10__box1">
			<ul>
				<li>
					<a href="#">
						JA全中
					</a>
				</li>
				<li>
					<a href="#">
						JA全共連
					</a>
				</li>
				<li>
					<a href="#">
						よつ葉乳業株式会社
					</a>
				</li>
				<li>
					<a href="#">
						（社）中央畜産会
					</a>
				</li>
				<li>
					<a href="#">
						田舎暮らしのススメ（鹿追町農業求人サイト）
					</a>
				</li>
			</ul>
		</div>
		<div class="l-flame10__box1">
			<ul>
				<li>
					<a href="#">
						JA全農
					</a>
				</li>
				<li>
					<a href="#">
						ホクレン農業協同組合連合会
					</a>
				</li>
				<li>
					<a href="#">
						牛乳に相談だ（中央酪農会議）
					</a>
				</li>
				<li>
					<a href="#">
						鹿追町デーリィサービスカンパニィ
					</a>
				</li>
				<li>
					<a href="#">
						JAカレッジ
					</a>
				</li>
			</ul>
		</div>
		<div class="l-flame10__box1 l-flame10__box1--margin">
			<ul>
				<li>
					<a href="#">
						JA北海道信連
					</a>
				</li>
				<li>
					<a href="#">
						アルーダ（中古農機情報）
					</a>
				</li>
				<li>
					<a href="#">
						社団法人全国農協乳業協会
					</a>
				</li>
				<li>
					<a href="#">
						ピュアモルト農業研修生（鹿追町役場）
					</a>
				</li>
			</ul>
		</div>
		
	</div>
</div>

</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>