<?php $id="page10";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a class="border" href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">
<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">フォトギャラリー</span><br>
	Photo Gallery
</div>

<?php
//==============================================
// c-tabs2 01
//============================================== ?>
<div class="l-content">
	<div class="c-tab2">
		<button class="tablinks2" onclick="openCity2(event, 'a')" id="defaultOpen2">
		畑作フォトギャラリー
		</button>
		<button class="tablinks2" onclick="openCity2(event, 'b')">
		酪農フォトギャラリー
		</button>
		<button class="tablinks2 tablinks2--margin" onclick="openCity2(event, 'c')">
		畜産フォトギャラリー
		</button>
	</div>
<?php
//==============================================
// c-tabs2 01 content1
//============================================== ?>
<div id="a" class="tabcontent2">
<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/01.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/01_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/02.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/02_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/03.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/03_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery01/04.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery01/thumb/04_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>
<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>



<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>
</div>
<?php
//==============================================
// c-tabs2 01 content2
//============================================== ?>
<div id="b" class="tabcontent2">
<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/01.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/01_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/02.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/02_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/03.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/03_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery02/04.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery02/thumb/04_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>


<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>



<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>
</div>
<?php
//==============================================
// c-tabs2 01 content2
//============================================== ?>
<div id="c" class="tabcontent2">
<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/01.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/thumb/01_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/02.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/thumb/02_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/03.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/thumb/03_thumb.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// c-nav5 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav5">
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
		<div class="c-nav5__box1  c-nav5__box1--margin"">
			<div class="c-nav5__img">
				<a class="example-image-link" href="assets/img/page10/gallery03/11.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page10/gallery03/11.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav5__text">
				キャプション
			</div>
		</div>
	</div>
</div>
</div>
</div>

</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>