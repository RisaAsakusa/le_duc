<?php $id="page9";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a class="border" href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">新着情報</span><br>
	News & Topics
</div>

<?php
//==============================================
// l-flame6 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame6">
		<div class="l-flame6__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame6__right">
			<div class="l-flame6__title">
				<span>トピックス</span>
				<span>2017.12.25</span>
			</div>
			<h2 class="u-title">タイトルタイトルタイトル</h2>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<h2 class="u-title2">外部リンク先タイトル</h2>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame6 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame6">
		<div class="l-flame6__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame6__right">
			<div class="l-flame6__title">
				<span>トピックス</span>
				<span>2017.12.25</span>
			</div>
			<h2 class="u-title">タイトルタイトルタイトル</h2>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<h2 class="u-title2">外部リンク先タイトル</h2>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame6 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame6">
		<div class="l-flame6__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame6__right">
			<div class="l-flame6__title l-flame6__title--color">
				<span>お知らせ</span>
				<span>017.12.20</span>
			</div>
			<h2 class="u-title">タイトルタイトルタイトル</h2>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
		</div>
		<div class="l-flame6__text2">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテ
				キストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキキストテキストテキス
				トテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
	</div>
</div>

<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn2">
		<div class="c-btn2__btn1">
			<a href="#">1</a>
		</div>
		<div class="c-btn2__btn1">
			<a href="#">2</a>
		</div>
		<div class="c-btn2__btn1 c-btn2__btn1--padding">
			<a href="#">次へ</a>
		</div>
	</div>
</div>






</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>