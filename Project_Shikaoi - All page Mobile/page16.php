<?php $id="page16";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a class="border" href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">募集要項</span><br>
	Recruitment
</div>

<?php
//==============================================
// c-table1
//============================================== ?>
<div class="l-content">
	<div class="c-table1">
		<table>
			<tr>
				<td>事業所名</td>
				<td>
					鹿追町農業協同組合
				</td>
			</tr>
			<tr>
				<td>所在地</td>
				<td>北海道河東郡鹿追町新町4丁目51番地</td>
			</tr>
			<tr>
				<td>全従業員数</td>
				<td>184名</td>
			</tr>
			<tr>
				<td>事業内容</td>
				<td>信用・共済・販売・購買・利用・指導</td>
			</tr>
			<tr>
				<td>採用職種</td>
				<td>正職員（総合職・農産技術職・酪農技術職）</td>
			</tr>
			<tr>
				<td>学歴</td>
				<td>大学卒（年齢不問）</td>
			</tr>
			<tr>
				<td>勤務時間</td>
				<td>
				■交代制 ： 無<br>
				【4月〜10月】　８：３０〜１７：００<br>
				【11月〜3月】　９：００〜１７：００<br>
				■休憩時間　６０分<br>
				■変形労働時間制 ： 有<br>
				■残業 ： 月平均４時間<br>
				</td>
			</tr>
			<tr>
				<td>給与</td>
				<td>
					■基本給 ： 184,000円（新卒者。経験者は別途対応）<br>
					■住宅手当 ： 4,700円
				</td>
			</tr>
			<tr>
				<td>賞与</td>
				<td>
					採用１年後<br>
					・　６月・・・基本給×1.05の1.85ヶ月<br>
					・　８月・・・燃料手当（1,866L分）<br>
					・１２月・・・基本給×1.05の1.9ヶ月<br>
					・１２月・・・基本給の1.2ヶ月<br>
					・　２月・・・基本給×1.05の0.55ヶ月<br>
					・特別賞与（業績による・前年実績・・・基本給×1.2ヶ月）
				</td>
			</tr>
			<tr>
				<td>休日</td>
				<td>
					日曜・祝日<br>
					5/1、6/15、8/15・16、12/29〜1/5<br>
					４週６休<br>
					指定休日 ： ７日<br>
					年間休日数 ： 100日
				</td>
			</tr>
			<tr>
				<td>加入保険</td>
				<td>
					健康・厚生・雇用・労災
				</td>
			</tr>
			<tr>
				<td>応募書類</td>
				<td>
					履歴書・ 卒業（見込）証明書・成績証明書　
				</td>
			</tr>
			<tr>
				<td>求人数</td>
				<td>
					2018年4月採用 1〜3名　
				</td>
			</tr>
		<tr>
				<td>説明会</td>
				<td>
					＊詳細はこちらのリクナビをご覧ください
				</td>
			</tr>
		</table>
	</div>
</div>

</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>