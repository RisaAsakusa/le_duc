<?php $id="page17";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a class="border" href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">個人情報保護方針</span><br>
	Privacy policy
</div>

<?php
//==============================================
// c-text3
//============================================== ?>
<div class="l-content">
	<div class="c-text3">
		<div class="c-text3__text1">
			<p>鹿追町農業協同組合（以下「当組合」といいます。）は、組合員・利用者等の皆様の個人情報を正しく取扱うことが当組合の事業活動の基本であり社会的責務であ
			ることを認識し、以下の方針を遵守することを誓約します。</p>
		</div>
		<div class="c-text3__text1">
			<p>１．当組合は、個人情報を適正に取扱うために、「個人情報の保護に関する法律」（以下「保護法」といいます。）その他、個人情報保護に関する関係諸法令およ
			び農林水産大臣をはじめ主務大臣のガイドライン等に定められた義務を誠実に遵守します。 個人情報とは、保護法第２条第１項に規定する、生存する個人に関する
			情報で、特定の個人を識別できるものをいい、以下も同様とします。 また、当組合は、特定個人情報を適正に取扱うために、「行政手続における特定の個人を識別
			するための番号の利用等に関する法律」（以下「番号法」といいます。）その他、特定個人情報の適正な取扱いに関する関係諸法令およびガイドライン等に定めら
			れた義務を誠実に遵守します。 特定個人情報とは、番号法第２条第８項に規定する、個人番号をその内容に含む個人情報をいい、以下も同様とします。
			</p>
		</div>
		<div class="c-text3__text1">
			<p>２．当組合は、個人情報の取扱いにおいて、利用目的をできる限り特定したうえ、あらかじめご本人の同意を得た場合および法令により例外として扱われるべき場
			合を除き、その利用目的の達成に必要な範囲内でのみ個人情報を利用します。ただし、特定個人情報においては、利用目的を特定し、ご本人の同意の有無に関わら
			ず、利用目的の範囲を超えた利用は行いません。 ご本人とは、個人情報によって識別される特定の個人をいい、以下も同様とします。利用目的は、法令により例外
			として扱われるべき場合を除き、あらかじめ公表するか、取得後速やかにご本人に通知し、または公表します。ただし、ご本人から直接書面で取得する場合には、
			あらかじめ明示します。</p>
		</div>
		<div class="c-text3__text1">
			<p>３．当組合は、個人情報を取得する際、適正かつ適法な手段で取得いたします。</p>
		</div>
		<div class="c-text3__text1">
			<p>４．当組合は、取扱う個人データ及び特定個人情報を利用目的の範囲内で正確・最新の内容に保つよう努め、また安全管理のために必要・適切な措置を講じ従業者
			および委託先を適正に監督します。 個人データとは、保護法第２条第４項が規定する、個人情報データベース等（保護法第２条第２項）を構成する個人情報をいい
			、以下同様とします。</p>
		</div>
		<div class="c-text3__text1">
			<p>５．当組合は、法令により例外として扱われるべき場合を除き、あらかじめご本人の同意を得ることなく、個人データを第三者に提供しません。 また、当組合は、
			番号法第19条各号により例外として扱われるべき場合を除き、ご本人の同意の有無に関わらず、特定個人情報を第三者に提供しません。</p>
		</div>
		<div class="c-text3__text1">
			<p>６．当組合は、ご本人の機微（センシティブ）情報（政治的見解、信教、労働組合への加盟、人種・民族、門地・本籍地、保健医療等に関する情報）については、
			法令等に基づく場合や業務遂行上必要な範囲においてご本人の同意をいただいた場合等を除き、取得・利用・第三者提供はいたしません。</p>
		</div>
		<div class="c-text3__text1">
			<p>７．当組合は、保有個人データにつき、法令に基づきご本人からの開示、訂正等に応じます。 保有個人データとは、保護法第２条第５項に規定するデータをいいま
			す。</p>
		</div>
		<div class="c-text3__text1">
			<p>８．当組合は、個人情報につき、ご本人からの苦情に対し迅速かつ適切に取り組み、そのための内部体制の整備に努めます</p>
		</div>
		<div class="c-text3__text1">
			<p>９．当組合は、個人情報について、適正な内部監査を実施するなどして、本保護方針の継続的な改善に努めます。
			</p><br>
		</div>
	</div>
</div>

<?php
//==============================================
// c-more1 01
//============================================== ?>
<div class="l-content">
	<a href="#"><div class="c-more1">
		個人情報保護に関する具体策についてはPDFファイルをご覧ください。
	</div></a>
</div>


</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>