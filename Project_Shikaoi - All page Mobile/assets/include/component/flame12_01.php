<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">サイトマップ </span><br>
	Site Map
</div>




<?php
//==============================================
// l-flame12 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame12">
		<div class="l-flame12__left">
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">JA鹿追町について</h2>
				</div>
				<div class="l-flame12__text">
					組合長からのご挨拶<br>
					JA綱領<br>
					基本理念<br>
					組織・組織構成図<br>
					職員募集のお知らせ
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">青年部・女性部・熟年会</h2>
				</div>
				<div class="l-flame12__text">
					青年部<br>
						<span class="u-margin">・活動日誌</span><br>
					女性部<br>
						<span class="u-margin">
							・活動日誌<br>
							・とっておきのレシピ
						</span><br>
					熟年会<br>
						<span class="u-margin">・活動日誌</span>
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">組合員情報</h2>
				</div>
				<div class="l-flame12__text">
					中古農機・自動車のご紹介
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">新着情報</h2>
				</div>
				<div class="l-flame12__text">
					お知らせ・トピックス
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">JAバンク</h2>
				</div>
				<div class="l-flame12__text">
					ＪＡバンク 苦情等対応要領の概要について<br>
					ＪＡバンク 苦情処理措置／紛争解決措置について
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">個人情報保護方針の取り扱い </h2>
				</div>
				<div class="l-flame12__text">
					個人情報保護方針の取り扱い
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">リンク</h2>
				</div>
				<div class="l-flame12__text">
					リンク
				</div>
			</div>
		</div>


		<div class="l-flame12__right">
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">鹿追町の農業</h2>
				</div>
				<div class="l-flame12__text">
					鹿追町の農業<br>
					コントラ事業
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">職場紹介</h2>
				</div>
				<div class="l-flame12__text">
					職場紹介
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">農業求人</h2>
				</div>
				<div class="l-flame12__text">
					職場紹介
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">JA通信しかおい</h2>
				</div>
				<div class="l-flame12__text">
					JA通信しかおい
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">JA共済</h2>
				</div>
				<div class="l-flame12__text">
					ＪＡ共済 苦情等対応要領の概要について<br>
					ＪＡ共済 苦情処理措置／紛争解決措置
				</div>
			</div>
			<div class="l-flame12__box1">
				<div class="l-flame12__title">
					<h2 class="c-more1">アクセスマップ</h2>
				</div>
				<div class="l-flame12__text">
					アクセスマップ
				</div>
			</div>
		</div>
	</div>
</div>