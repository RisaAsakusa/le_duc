<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">リンク </span><br>
	Links
</div>


<?php
//==============================================
// l-flame10 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame10">
		<div class="l-flame10__box1">
			<ul>
				<li>
					<a href="#">
						JA全中
					</a>
				</li>
				<li>
					<a href="#">
						JA全共連
					</a>
				</li>
				<li>
					<a href="#">
						よつ葉乳業株式会社
					</a>
				</li>
				<li>
					<a href="#">
						（社）中央畜産会
					</a>
				</li>
				<li>
					<a href="#">
						田舎暮らしのススメ（鹿追町農業求人サイト）
					</a>
				</li>
			</ul>
		</div>
		<div class="l-flame10__box1">
			<ul>
				<li>
					<a href="#">
						JA全農
					</a>
				</li>
				<li>
					<a href="#">
						ホクレン農業協同組合連合会
					</a>
				</li>
				<li>
					<a href="#">
						牛乳に相談だ（中央酪農会議）
					</a>
				</li>
				<li>
					<a href="#">
						鹿追町デーリィサービスカンパニィ
					</a>
				</li>
				<li>
					<a href="#">
						JAカレッジ
					</a>
				</li>
			</ul>
		</div>
		<div class="l-flame10__box1 l-flame10__box1--margin">
			<ul>
				<li>
					<a href="#">
						JA北海道信連
					</a>
				</li>
				<li>
					<a href="#">
						アルーダ（中古農機情報）
					</a>
				</li>
				<li>
					<a href="#">
						社団法人全国農協乳業協会
					</a>
				</li>
				<li>
					<a href="#">
						ピュアモルト農業研修生（鹿追町役場）
					</a>
				</li>
			</ul>
		</div>
		
	</div>
</div>