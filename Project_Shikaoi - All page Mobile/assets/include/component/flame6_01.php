<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">新着情報</span><br>
	News & Topics
</div>


<?php
//==============================================
// l-flame6 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame6">
		<div class="l-flame6__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame6__right">
			<div class="l-flame6__title">
				<span>トピックス</span>
				<span>2017.12.25</span>
			</div>
			<h2 class="u-title">タイトルタイトルタイトル</h2>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<h2 class="u-title2">外部リンク先タイトル</h2>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame6 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame6">
		<div class="l-flame6__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame6__right">
			<div class="l-flame6__title">
				<span>トピックス</span>
				<span>2017.12.25</span>
			</div>
			<h2 class="u-title">タイトルタイトルタイトル</h2>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<h2 class="u-title2">外部リンク先タイトル</h2>
		</div>
	</div>
</div>


<?php
//==============================================
// l-flame6 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame6">
		<div class="l-flame6__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame6__right">
			<div class="l-flame6__title l-flame6__title--color">
				<span>お知らせ</span>
				<span>017.12.20</span>
			</div>
			<h2 class="u-title">タイトルタイトルタイトル</h2>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
			<div class="l-flame6__text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
			</div>
		</div>
		<div class="l-flame6__text2">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテ
				キストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキキストテキストテキス
				トテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
	</div>
</div>
<?php
//==============================================
// c-btn1
//============================================== ?>
<div class="l-content">
	<div class="c-btn2">
		<div class="c-btn2__btn1">
			<a href="#">1</a>
		</div>
		<div class="c-btn2__btn1">
			<a href="#">2</a>
		</div>
		<div class="c-btn2__btn1 c-btn2__btn1--padding">
			<a href="#">次へ</a>
		</div>
	</div>
</div>