<?php
//==============================================
// c-nav1 01
//============================================== ?>
<section class="option3">
		<div class="l-content">
		<div class="c-nav1">
			<div class="c-nav1__box1">
				<div class="c-nav1__img">
					<img src="assets/img/page1/shikaoi_brand01.jpg" alt="">
				</div>
				<div class="c-nav1__text">
					<h2>肥沃な大地で生産する安全な農作物</h2>
					テキストテキストテキストテキストテキストテキストテキスト
					テキストテキストテキストテキストテキスト
				</div>
			</div>
			<div class="c-nav1__box1 c-nav1__box1--margin">
				<div class="c-nav1__img">
					<img src="assets/img/page1/shikaoi_brand02.jpg" alt="">
				</div>
				<div class="c-nav1__text">
					<h2>北海道を代表する高品質の牛乳</h2>
					テキストテキストテキストテキストテキストテキストテキスト
					テキストテキストテキストテキストテキスト
				</div>
			</div>
			<div class="c-nav1__box1">
				<div class="c-nav1__img">
					<img src="assets/img/page1/shikaoi_brand03.jpg" alt="">
				</div>
				<div class="c-nav1__text">
					<h2>信頼の「鹿追ブランド」牛肉・豚肉</h2>
					テキストテキストテキストテキストテキストテキストテキスト
					テキストテキストテキストテキストテキスト
				</div>
			</div>
		</div>
		</div>
	</section>
<?php
//==============================================
// .c-title1
//============================================== ?>
<div class="c-title1">
	<span class="u-size2">青年部・女性部・熟年会</span><br>
	Shikaoi Community
</div>
<?php
//==============================================
// c-nav1 02
//============================================== ?>
<section class="option3">
		<div class="l-content">
		<div class="c-nav1">
			<div class="c-nav1__box1">
				<div class="c-nav1__img">
					<img src="assets/img/page1/shikaoi_brand04.jpg" alt="">
				</div>
				<div class="c-nav1__text">
					<h2>青年部</h2>
					テキストテキストテキストテキストテキストテキストテキスト
					テキストテキストテキストテキストテキスト
				</div>
			</div>
			<div class="c-nav1__box1 c-nav1__box1--margin">
				<div class="c-nav1__img">
					<img src="assets/img/page1/shikaoi_brand05.jpg" alt="">
				</div>
				<div class="c-nav1__text">
					<h2>女性部</h2>
					テキストテキストテキストテキストテキストテキストテキスト
					テキストテキストテキストテキストテキスト
				</div>
			</div>
			<div class="c-nav1__box1">
				<div class="c-nav1__img">
					<img src="assets/img/page1/shikaoi_brand06.jpg" alt="">
				</div>
				<div class="c-nav1__text">
					<h2>熟年会</h2>
					テキストテキストテキストテキストテキストテキストテキスト
					テキストテキストテキストテキストテキスト
				</div>
			</div>
		</div>
		</div>
	</section>