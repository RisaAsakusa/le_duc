
<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">青年部</span><br>
	Youth Community 
</div>


<?php
//==============================================
// l-flame3 02
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__right">
			<div class="l-flame3__text2">
				<h2>タイトル タイトル</h2>
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				 
			</div>
			<div class="l-flame3__text2">
				<h2>タイトル タイトル</h2>
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたします。
			</div>
			<div class="l-flame3__btn1">
				<a href="#">
					<div class="c-btn1">
						青年部活動日誌はこちら
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">女性部</span><br>
	Ladies Community 
</div>


<?php
//==============================================
// l-flame3 02
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__right">
			<div class="l-flame3__text2">
				<h2>タイトル タイトル</h2>
				女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
				女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
				女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。 
			</div>
			<div class="l-flame3__text2">
				<h2>タイトル タイトル</h2>
				女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
				女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。
				女性部の紹介テキストをお願いいたします。女性部の紹介テキストをお願いいたします。 
			</div>
			<div class="l-flame3__btn1">
				<a href="#">
					<div class="c-btn1">
						女性部活動日誌はこちら
					</div>
				</a>
				<a href="#">
					<div class="c-btn1 c-btn1--color1">
						とっておきのレシピはこちら
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">熟年会</span><br>
	Senior Community
</div>


<?php
//==============================================
// l-flame3 02
//============================================== ?>
<div class="l-content">
	<div class="l-flame3">
		<div class="l-flame3__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame3__right">
			<div class="l-flame3__text2">
				<h2>タイトル タイトル</h2>
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたし
				ます。青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願い
				いたします。 
			</div>
			<div class="l-flame3__text2">
				<h2>タイトル タイトル</h2>
				青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願いいたし
				ます。青年部の紹介テキストをお願いいたします。青年部の紹介テキストをお願い
				いたします。青年部の紹介テキストをお願いいたします。青年部の紹介テキストを
				お願いいたします。青年部の紹介テキストをお願いいたします。青年部の紹介テキ
				ストをお願いいたします。
			</div>
			<div class="l-flame3__btn1">
				<a href="#">
					<div class="c-btn1">
						熟年会活動日誌はこちら
					</div>
				</a>
			</div>
		</div>
	</div>
</div>