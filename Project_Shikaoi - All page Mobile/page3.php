<?php $id="page3";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a class="border" href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">
	<?php
	//==============================================
	// .c-title2 01
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">鹿追町の農業</span><br>
		概要
	</div>

	<?php
	//==============================================
	// c-img1 01
	//============================================== ?>
	<div class="l-content">
		<div class="c-img1">
			<img src="assets/img/page3/shikaoi.jpg" alt="">
		</div>
	</div>

	<?php
	//==============================================
	// c-text1
	//============================================== ?>
	<div class="l-content">
		<div class="c-text1">
			<div class="c-text1__text1">
				　鹿追町は北海道の一大農業地帯十勝平野の北西端に位置し、大雪山国立公園の一部を含む夫婦山のふもとに広がる山麓農村地帯です。人口は、5,598人（総世帯数2,444戸）<br>
				うち農家人口約1,200人（農家戸数237戸、うち法人23戸）総面積40,469haうち農地面積11,512ha で全体の28.4％に当たり、大部分は国有林を中心とする山林地帯となっ
				ています。
			</div>
			<div class="c-text1__text1">
				　鹿追町の基幹産業である農業は、畑作・酪農（畜産）を中心として重要な役割を果たしています。今年度から第9次農業振興計画がスタートし、今までの鹿追町農業を継承
				しつつ新たな時代を見据えた事業に取り組んでいます。
			</div>
			<div class="c-text1__text1">
				　畑作では、輪作体系の確立を基本に堆肥投入・緑肥栽培・交換耕作等の地力対策やマッピングシステムを活用した施肥設計、生産履歴のデータ集積・解析とを合わせ経営分
				析システムを活用しコスト低減を図っています。また、主要作物の小麦・てん菜・馬鈴しょ・豆類は適期管理作業等の励行による高品質・生産性向 上を図りつつ野菜等も取
				り入れ所得向上を目指しています。
			</div>
			<div class="c-text1__text1">
				　酪農では、「土づくり」「草づくり」「牛づくり」を基本に土壌分析に基づく適正施肥により良質粗飼料の確保、乳牛遺伝改良等を推進し生産性向上を図っています。更に
				、哺育から初妊までの一貫預託による労働力の軽減と後継牛の資質向上を図ると共に、コントラクター事業に良質粗飼料の収穫を委託することにより飼養管理作業の徹底を
				図りゆとりある経営を行っています。また、肉牛は鹿追町内で生産される乳雄仔牛（交雑種を含む）のみを「一貫肥育」しており、年間約5,300頭を出荷し、「鹿追生まれ
				・鹿追育ちの町内完結型」生産体系を確立しています。
			</div>
			<div class="c-text1__text1">
				　この他農村女性や青年部等農業後継者の積極的経営参画に向け支援を行っています。営農経済面においては、経営コンサルシステムの活用により営農計画書の作成相談によ
				る戸別経営体の課題改善等を行い、経営の安定と更なる向上を目指しています。
			</div>
		</div>
	</div>
	<div class="l-content">
		<a href="#"><span class="c-more">詳細のPDFファイルはこちら</span></a><br>
		<a href="#"><span class="c-more">フォトギャラリーはこちら</span></a>
	</div>
	
	<?php
	//==============================================
	// .c-title2
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">コントラ事業</span><br>
		飼料作物管理事業
	</div>

	<?php
	//==============================================
	// l-flame3 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame3">
			<div class="l-flame3__left">
				<div class="c-slider1">
					<div class="flexslider1">
						<ul class="slides">
					     	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
					      	<li>
					        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
					      	</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="l-flame3__right">
				<div class="l-flame3__text1">
					ＪＡ事業としては、全国で最初に設立され、コントラ課に委託した作業の軽減によ り、労働強化することなく、酪農の規模拡大と生乳生産の拡大が実現出来ました。 現在の酪農家1戸当たりの乳牛飼養頭数は約150頭と他地域を大きく上回っています。
				</div>
				<div class="l-flame3__text1">
					自給飼料生産の拡大と品質および栄養価の向上ならびに自給飼料生産コストが低減し、 堆肥等の圃場還元面積と量が増加し、購入肥料代が減少しました。結果として、環境保全型農業の進展を促しています。
				</div>
				<div class="l-flame3__text1">
					このコントラ事業のおかげで、酪農は、「土づくり」「草づくり」を基本とし、 良質粗飼料の確保・給与、新技術導入、乳牛遺伝改良を促進し生産性向上を図っています。酪農経営の安定と労働緩和を図り生活にゆとりを持たせる重要な事業として、組合員の経営に定着化しています。
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>