<?php $id="page22";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a class="border" href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">
	<?php
	//==============================================
	// .c-title2
	//============================================== ?>
	<div class="c-title1 c-title1--size">
		<span class="u-size2">サイトマップ </span><br>
		Site Map
	</div>
	<?php
	//==============================================
	// l-flame12 01
	//============================================== ?>
	<div class="l-content">
		<div class="l-flame12">
			<div class="l-flame12__left">
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">JA鹿追町について</h2>
					</div>
					<div class="l-flame12__text">
						組合長からのご挨拶<br>
						JA綱領<br>
						基本理念<br>
						組織・組織構成図<br>
						職員募集のお知らせ
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">青年部・女性部・熟年会</h2>
					</div>
					<div class="l-flame12__text">
						青年部<br>
							<span class="u-margin">・活動日誌</span><br>
						女性部<br>
							<span class="u-margin">
								・活動日誌<br>
								・とっておきのレシピ
							</span><br>
						熟年会<br>
							<span class="u-margin">・活動日誌</span>
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">組合員情報</h2>
					</div>
					<div class="l-flame12__text">
						中古農機・自動車のご紹介
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">新着情報</h2>
					</div>
					<div class="l-flame12__text">
						お知らせ・トピックス
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">JAバンク</h2>
					</div>
					<div class="l-flame12__text">
						ＪＡバンク 苦情等対応要領の概要について<br>
						ＪＡバンク 苦情処理措置／紛争解決措置について
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">個人情報保護方針の取り扱い </h2>
					</div>
					<div class="l-flame12__text">
						個人情報保護方針の取り扱い
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">リンク</h2>
					</div>
					<div class="l-flame12__text">
						リンク
					</div>
				</div>
			</div>


			<div class="l-flame12__right">
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">鹿追町の農業</h2>
					</div>
					<div class="l-flame12__text">
						鹿追町の農業<br>
						コントラ事業
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">職場紹介</h2>
					</div>
					<div class="l-flame12__text">
						職場紹介
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">農業求人</h2>
					</div>
					<div class="l-flame12__text">
						職場紹介
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">JA通信しかおい</h2>
					</div>
					<div class="l-flame12__text">
						JA通信しかおい
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">JA共済</h2>
					</div>
					<div class="l-flame12__text">
						ＪＡ共済 苦情等対応要領の概要について<br>
						ＪＡ共済 苦情処理措置／紛争解決措置
					</div>
				</div>
				<div class="l-flame12__box1">
					<div class="l-flame12__title">
						<h2 class="c-more1">アクセスマップ</h2>
					</div>
					<div class="l-flame12__text">
						アクセスマップ
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>