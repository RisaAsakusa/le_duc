<?php $id="page4";?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/common.css" rel="stylesheet">
<link href="assets/css/index.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/css/lightbox.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider-min.css'>
<script src="assets/js/common.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header>
	<div class="c-header">
		<div class="c-infohd">
			平成29年度スローガン「農〜 魅せる〜」
		</div>
	</div>
	<div class="c-gnavi pc">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<nav class="c-menu">
			<ul>
				<li><a href="index.php">ホーム</a></li>
				<li><a href="page2.php">JA鹿追町について</a></li>
				<li><a href="page3.php">鹿追町の農業</a></li>
				<li><a class="border" href="page4.php">青年部・女性部・熟年会</a></li>
				<li><a href="page5.php">職場紹介</a></li>
				<li><a href="page6.php">組合員情報</a></li>
				<li><a href="page7.php">農業求人</a></li>
				<li><a href="page8.php">新着情報</a></li>
			</ul>
		</nav>
	</div>
	<div class="c-gnavi sp">
		<div class="c-logo">
			<a href="index.php"><img src="assets/img/logo.PNG" alt=""></a>
		</div>
		<div class="menu_sp">
			<div class="iconmenu" onclick="myFunction(this)">
			  <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
			</div>
		</div>
	</div>
	<nav class="c-menu_sp">
		<ul>
			<li><a href="index.php">ホーム</a></li>
			<li><a href="page2.php">JA鹿追町について</a></li>
			<li><a href="page3.php">鹿追町の農業</a></li>
			<li><a href="page4.php">青年部・女性部・熟年会</a></li>
			<li><a href="page5.php">職場紹介</a></li>
			<li><a href="page6.php">組合員情報</a></li>
			<li><a href="page7.php">農業求人</a></li>
			<li><a href="page8.php">新着情報</a></li>
		</ul>
	</nav>

</header>
<div class="container">
<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">とっておきのレシピ</span><br>
	My favorite Recipe 
</div>

<?php
//==============================================
// .c-title4
//============================================== ?>
<div class="c-title1 c-title1--size4">
	鹿追産の食材を使ったお料理<br>
	<span class="u-size4">カボカレーコロッケ</span>
</div>

<?php
//==============================================
// l-flame8 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame8">
		<div class="l-flame8__left">
			<img src="assets/img/page15/recipe_84.jpg" alt="">
			<div class="l-flame8__color">
				ワンポイント
			</div>
			<div class="l-flame8__text">
				料理のコツやワンポイントアドバイスを掲載。料理のコツやワンポイントアドバ
				イスを掲載。料理のコツやワンポイントアドバイスを掲載。料理のコツやワンポ
				イントアドバイスを掲載。料理のコツやワンポイントアドバイスを掲載。
			</div>
		</div>
		<div class="l-flame8__right">
			<table>
				<tr>
					<th>材料・調味料</th>
					<th>分量</th>
					<th>備考</th>
				</tr>
				<tr>
					<td>カボチャ</td>
					<td>1個</td>
					<td></td>
				</tr>
				<tr>
					<td>バター</td>
					<td>小さじ1</td>
					<td></td>
				</tr>
				<tr>
					<td>塩コショウ</td>
					<td>少々</td>
					<td></td>
				</tr>
				<tr>
					<td>カレーパウダー</td>
					<td>大さじ3</td>
					<td></td>
				</tr>
				<tr>
					<td>ピザ用チーズ</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td>（衣）卵</td>
					<td>1個</td>
					<td></td>
				</tr>
				<tr>
					<td>水</td>
					<td>50cc</td>
					<td></td>
				</tr>
				<tr>
					<td>薄力粉</td>
					<td>大さじ4</td>
					<td></td>
				</tr>
				<tr>
					<td>パン粉</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td>黒ゴマ</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td>揚げ油</td>
					<td>適量</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>作り方</h2>
</div>

<?php
//==============================================
// l-flame9 01
//============================================== ?>
<div class="l-content">
	<div class="l-flame9">
		<div class="l-flame9__box1">
			<h2>STEP 1</h2>
			<p>カボチャは適当な大きさに切って種を取り、皮をむく。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 2</h2>
			<p>鍋に（1）のカボチャとかぶるくらいの水、バターを入
れ、火にかけて柔らかくなるまで煮る。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 3</h2>
			<p>串が通る程度に柔らかくなったら煮汁を捨て、再び火
にかけて水気を飛ばす。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 4</h2>
			<p>（3）を人肌くらいまで冷ましたら、塩コショウ、カレー
パウダーで調味しながら粒が残る程度に潰しておく。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 5</h2>
			<p>（4）を好みの大きさに取り、ピザ用チーズを包み込む
ように形作る。20分たったら火をとめ粗熱をとり保存
袋に入れる。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 6</h2>
			<p>（5）に衣を付ける。卵と水は合わせて溶き、薄力粉
を加えて混ぜる。（5）を卵液にくぐらせ、黒ゴマを
加えたパン粉を付ける。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 7</h2>
			<p>（3）を人肌くらいまで冷ましたら、塩コショウ、カレー
パウダーで調味しながら粒が残る程度に潰しておく。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 8</h2>
			<p>（4）を好みの大きさに取り、ピザ用チーズを包み込む
ように形作る。20分たったら火をとめ粗熱をとり保存
袋に入れる。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 9</h2>
			<p>（5）に衣を付ける。卵と水は合わせて溶き、薄力粉
を加えて混ぜる。（5）を卵液にくぐらせ、黒ゴマを
加えたパン粉を付ける。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 10</h2>
			<p>（3）を人肌くらいまで冷ましたら、塩コショウ、カレー
パウダーで調味しながら粒が残る程度に潰しておく。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 11</h2>
			<p>（4）を好みの大きさに取り、ピザ用チーズを包み込む
ように形作る。20分たったら火をとめ粗熱をとり保存
袋に入れる。</p>
		</div>
		<div class="l-flame9__box1">
			<h2>STEP 12</h2>
			<p>（5）に衣を付ける。卵と水は合わせて溶き、薄力粉
を加えて混ぜる。（5）を卵液にくぐらせ、黒ゴマを
加えたパン粉を付ける。</p>
		</div>
	</div>
</div>

<?php
//==============================================
// .c-title2
//============================================== ?>
<div class="c-title1 c-title1--size">
	<span class="u-size2">おすすめレシピ</span><br>
	Archive 
</div>

<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>和食</h2>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>




<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>洋食・中華</h2>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>



<?php
//==============================================
// .c-titlee
//============================================== ?>
<div class="c-title5">
	<h2>デザート・ケーキ</h2>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>

<?php
//==============================================
// c-nav7 01
//============================================== ?>
<div class="l-content">
	<div class="c-nav7">
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		<div class="c-nav7__box1 c-nav7__box1--margin">
			<div class="c-nav7__img">
				<a class="example-image-link" href="assets/img/page15/recipe_84.jpg" data-lightbox="example-1">
				<img class="example-image" src="assets/img/page15/recipe_thumb01.jpg" alt="image-1" /></a>
			</div>
			<div class="c-nav7__text">
				レシピ名レシピ名レシピ名レシピ<br>
				レシピ名レシピ名レシピ名レシピ
			</div>
		</div>
		
	</div>
</div>






</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>