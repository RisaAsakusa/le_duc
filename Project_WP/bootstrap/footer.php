<footer class="py-5 bg-dark">
	<div class="container">
		<p class="m-0 text-center text-white">
			Copyright &copy; <?php bloginfo( 'sitename' ); ?> <?php echo date('Y'); ?></p>
	</div>
</footer>
</div> <!--end container -->
<?php wp_footer(); ?>
</body>
</html>