<?php get_header(); ?>
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <h1 class="mt-4"><?php the_title(); ?></h1>
          <p class="lead">
           <?php printf(' by <a href="%1$s">%2$s</a>', get_author_posts_url( get_the_author_meta('ID') ), get_the_author() ); ?>
          </p>
          <hr><p>Posted on <?php the_date(); ?> at <?php the_time('g:i a'); ?></p><hr>
           <?php if(has_post_thumbnail(  )): ?>
                  <img class="img-fluid rounded" src="<?php the_post_thumbnail_url('full');?>" alt="">
                <?php endif; ?>
          <hr><p class="lead"><?php the_content(); ?></p><hr>
          <?php
          if ( comments_open() || get_comments_number() ) :
            comments_template();
          endif;
           ?>
           <?php endwhile; ?>
          <?php else : ?>
                  <?php _e( 'Sorry, Nothing Found', 'bootstrap' ); ?>
          <?php endif; ?>
        </div>

        <div class="col-md-4">
           <?php get_sidebar(); ?>
        </div>
      </div>
    </div>
<?php get_footer(); ?>