<?php
/**
  @ Thiết lập các hằng dữ liệu quan trọng
  @ THEME_URL = get_stylesheet_directory() - đường dẫn tới thư mục theme
  @ CORE = thư mục /core của theme, chứa các file nguồn quan trọng.
  **/
  define( 'THEME_URL', get_stylesheet_directory() );
  define( 'CORE', THEME_URL . '/core' );
/**
  @ Load file /core/init.php
  @ Đây là file cấu hình ban đầu của theme mà sẽ không nên được thay đổi sau này.
  **/
  require_once( CORE . '/init.php' );
 /**
  @ Thiết lập $content_width để khai báo kích thước chiều rộng của nội dung
  **/
  if ( ! isset( $content_width ) ) {
        /*
         * Nếu biến $content_width chưa có dữ liệu thì gán giá trị cho nó
         */
        $content_width = 1200;
   }
/**
  @ Thiết lập các chức năng sẽ được theme hỗ trợ
  **/
  if ( ! function_exists( 'wordpress_theme_setup' ) ) {
        /*
         * Nếu chưa có hàm wordpress_theme_setup thì sẽ tạo mới hàm đó
         */
        function wordpress_theme_setup() {
                /*
                 * Thiết lập theme có thể dịch được
                 */
                $language_folder = THEME_URL . '/languages';
                load_theme_textdomain( 'bootstrap', $language_folder );
                /*
                 * Tự chèn RSS Feed links trong <head>
                 */
                add_theme_support( 'automatic-feed-links' );
                /*
                 * Thêm chức năng post thumbnail
                 */
                add_theme_support( 'post-thumbnails' );
                /*
                 * Thêm chức năng title-tag để tự thêm <title>
                 */
                add_theme_support( 'title-tag' );
                /*
                 * Thêm chức năng post format
                 */
                add_theme_support( 'post-formats',
                        array(
                                'video',
                                'image',
                                'audio',
                                'gallery'
                        )
                 );
                /*
                 * Thêm chức năng custom background
                 */
                $default_background = array(
                        'default-color' => '#e8e8e8',
                );
                add_theme_support( 'custom-background', $default_background );
                /*
                 * Tạo menu cho theme
                 */
                 register_nav_menu ( 'primary-menu', __('Primary Menu', 'bootstrap') );
                /*
                 * Tạo sidebar cho theme
                 */
                 $sidebar = array(
                    'name' => __('Main Sidebar', 'bootstrap'),
                    'id' => 'main-sidebar',
                    'description' => 'Main sidebar for bootstrap theme',
                    'class' => 'main-sidebar',
                    'before_title' => '<h3 class="widgettitle">',
                    'after_sidebar' => '</h3>'
                 );
                 register_sidebar( $sidebar );
        }
        add_action ( 'init', 'wordpress_theme_setup' );
	}
/**@ Thiết lập hàm hiển thị logo@ vanduc_logo()**/
if ( ! function_exists( 'vanduc_logo' ) ) {
  function vanduc_logo() {?>
        <?php
          printf(
            '<a class="navbar-brand" href="%1$s" title="%2$s">%3$s</a>',
            get_bloginfo( 'url' ),
            get_bloginfo( 'description' ),
            get_bloginfo( 'sitename' )
          );
       ?>
  <?php }
}
/**
@ Thiết lập hàm hiển thị menu
@ vanduc_menu( $slug )
**/
if ( ! function_exists( 'vanduc_menu' ) ) {
  function vanduc_menu( $menu ) {
    $menu = array(
      'theme_location' => $menu,
      'container' => 'ul',
      'menu_class' => 'navbar-nav ml-auto'
    );
    wp_nav_menu( $menu );
  }
}
/**
@ Chèn CSS và Javascript vào theme
@ sử dụng hook wp_enqueue_scripts() để hiển thị nó ra ngoài front-end
**/
function vanduc_styles() {
  /*
   * Hàm get_stylesheet_uri() sẽ trả về giá trị dẫn đến file style.css của theme
   * Nếu sử dụng child theme, thì file style.css này vẫn load ra từ theme mẹ
   */
  wp_register_style( 'main-style', get_template_directory_uri() . '/style.css', 'all' );
  wp_enqueue_style( 'main-style' );
  wp_register_style( 'styles-bootstrap',get_template_directory_uri() .'/vendor/bootstrap/css/bootstrap.min.css', 'all' );
  wp_enqueue_style( 'styles-bootstrap' );
  wp_register_style( 'home-style', get_template_directory_uri() . '/css/blog-home.css', 'all' );
  wp_enqueue_style( 'home-style' );
  wp_register_script('jquery-script', get_template_directory_uri() . "/vendor/jquery/jquery.min.js", array('jquery'));
	  wp_enqueue_script('jquery-script');
  wp_register_script('bootstrap-script', get_template_directory_uri() . "/vendor/bootstrap/js/bootstrap.bundle.min.js", array('jquery'));
	  wp_enqueue_script('bootstrap-script');
}
add_action( 'wp_enqueue_scripts', 'vanduc_styles' );


/**
@ Tạo hàm phân trang cho index, archive.
@ Hàm này sẽ hiển thị liên kết phân trang theo dạng chữ: Newer Posts & Older Posts
@ vanduc_pagination()
**/
if ( ! function_exists( 'vanduc_pagination' ) ) {
  function vanduc_pagination() {
    /*
     * Không hiển thị phân trang nếu trang đó có ít hơn 2 trang
     */
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
      return '';
    }
  ?>
  <nav class="pagination" role="navigation">
    <?php if ( get_next_post_link() ) : ?>
      <div class="prev"><?php next_posts_link( __('Older Posts', 'bootstrap') ); ?></div>
    <?php endif; ?>
    <?php if ( get_previous_post_link() ) : ?>
      <div class="next"><?php previous_posts_link( __('Newer Posts', 'bootstrap') ); ?></div>
    <?php endif; ?>
  </nav><?php
  }
}
/**
@ Hàm hiển thị tiêu đề của post trong .entry-header
@ Tiêu đề của post sẽ là nằm trong thẻ <h1> ở trang single
@ Còn ở trang chủ và trang lưu trữ, nó sẽ là thẻ <h2>
@ vanduc_entry_header()
**/
if ( ! function_exists( 'vanduc_entry_header' ) ) {
  function vanduc_entry_header() {
    if ( is_single() ) : ?>
 
      <h1 class="card-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h1>
    <?php else : ?>
      <h2 class="card-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h2><?php
 
    endif;
  }
}

/**
@ Hàm hiển thị thông tin của post (Post Meta)
@ vanduc_entry_meta()
**/
if( ! function_exists( 'vanduc_entry_meta' ) ) {
  function vanduc_entry_meta() {
    if ( ! is_page() ) :
      echo '<div class="card-footer text-muted">';
        // Hiển thị tên tác giả, tên category và ngày tháng đăng bài
        printf( __('<span class="author">Posted by %1$s</span>', 'bootstrap'),
          get_the_author() );
        printf( __('<span class="date-published"> at %1$s</span>', 'bootstrap'),
          get_the_date() );
        printf( __('<span class="category"> in %1$s</span>', 'bootstrap'),
          get_the_category_list( ', ' ) );
        // Hiển thị số đếm lượt bình luận
        if ( comments_open() ) :
          echo ' <span class="meta-reply">';
            comments_popup_link(
              __('Leave a comment', 'bootstrap'),
              __('One comment', 'bootstrap'),
              __('% comments', 'bootstrap'),
              __('Read all comments', 'bootstrap')
             );
          echo '</span>';
        endif;
      echo '</div>';
    endif;
  }
}


/**
@ Hàm hiển thị nội dung của post type
@ Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
@ Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
@ thachpham_entry_content()
**/
if ( ! function_exists( 'vanduc_entry_content' ) ) {
  function vanduc_entry_content() {
    
    if ( ! is_single() ) :
      the_excerpt();
    else :
      the_content();
 
      /*
       * Code hiển thị phân trang trong post type
       */
      $link_pages = array(
        'before' => __('<p>Page:', 'bootstrap'),
        'after' => '</p>',
        'nextpagelink'     => __( 'Next page', 'bootstrap' ),
        'previouspagelink' => __( 'Previous page', 'bootstrap' )
      );
      wp_link_pages( $link_pages );
     
    endif;
  }
}

/*
 * Thêm chữ Read More vào excerpt
 */
function vanduc_readmore() {

  return '...<br><br><br><a class="btn btn-primary" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More →', 'bootstrap') . '</a>';
}
add_filter( 'excerpt_more', 'vanduc_readmore' );

/**
@ Hàm hiển thị tag của post
@ vanduc_entry_tag()
**/
if ( ! function_exists( 'vanduc_entry_tag' ) ) {
  function vanduc_entry_tag() {
    if ( has_tag() ) :
      echo '<div class="entry-tag">';
      printf( __('Tagged in %1$s', 'bootstrap'), get_the_tag_list( '', ', ' ) );
      echo '</div>';
    endif;
  }
}

