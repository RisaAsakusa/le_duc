<!DOCTYPE html>
<html <?php language_attributes(); ?>
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<meta name="description" content="">
    	<meta name="author" content="">
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <link rel="profile" href="http://gmgp.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	    <div class="container">
	    	<?php vanduc_logo(); ?>
	    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
	          <span class="navbar-toggler-icon"></span>
	        </button>
	    <div class="collapse navbar-collapse" id="navbarResponsive">
	    	<?php vanduc_menu( 'primary-menu' ); ?>
	    </div>
    </nav>
	