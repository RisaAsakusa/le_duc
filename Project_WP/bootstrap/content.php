<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h1 class="card-title"><?php the_title();?></h1>
			<div class="card mb-4">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			        <?php if(has_post_thumbnail(  )): ?>
		                <img class="card-img-top" src="<?php the_post_thumbnail_url('full');?>" alt="Card image cap">
		            <?php endif; ?>
			        <div class="card-body">
			        	<?php vanduc_entry_header(); ?>
				        <div class="card-text">
							<?php vanduc_entry_content(); ?>
				       		<?php ( is_single() ? vanduc_entry_tag() : '' ); ?>
				        </div>
			        </div>
			        <?php vanduc_entry_meta() ?>
				</article>
			</div>
		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

