<div class="l-content">
	<div class="l-flame4">
		<div class="l-flame4__left">
			<div class="c-slider1">
				<div class="flexslider1">
					<ul class="slides">
				     	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
				      	<li>
				        	<a href="#" class="flex-active"><img src="assets/img/page3/img_slide1.png" alt=""></a>
				      	</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="l-flame4__right">
			<table>
				<tr>
					<td>メーカー名</td>
					<td>
						<span class="td1">三菱</span>
						<span class="td2">形式</span>
						<span class="td1">XXX-15</span>
					</td>
				</tr>
				<tr>
					<td>年式</td>
					<td>XXX-XXX</td>
				</tr>
				<tr>
					<td>本体価格</td>
					<td>1,000,000円（税別）</td>
				</tr>
				<tr>
					<td>整備状況</td>
					<td>整備済み</td>
				</tr>
				<tr>
					<td>付属品</td>
					<td>無し</td>
				</tr>
				<tr>
					<td>消耗品</td>
					<td>タイヤ8部山</td>
				</tr>
				<tr>
					<td>お問い合わせ</td>
					<td>担当 ◯◯◯まで。tel 0000-00-0000</td>
				</tr>
				<tr>
					<td>詳細説明</td>
					<td>説明テキスト。説明テキスト。説明テキスト。説明テキ
					スト。説明テキスト。説明テキスト。説明テキスト。説
					明テキスト。説明テキスト。説明テキスト。</td>
				</tr>
			</table>

		</div>
	</div>
</div>