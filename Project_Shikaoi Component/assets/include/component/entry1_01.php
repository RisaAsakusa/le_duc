<section class="option5">
		<div class="l-content">
		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/acoop_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				Aコープ鹿追店
				<h2>国産野菜統一宣言！ 地産地消をおいしく応援中。</h2>
				<p>Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>
				安心で安全な食品をご提供いたします。</p>
			</div>
		</div>

		<div class="c-entry1">
			<div class="c-entry1__img">
				<img src="assets/img/page1/furusato_banner.jpg" alt="">
			</div>
			<div class="c-entry1__text">
				Aコープ鹿追店 <span class="u-bgred">NEW</span>
				<h2>国産野菜統一宣言！ 地産地消をおいしく応援中。</h2>
				<p>Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>
				安心で安全な食品をご提供いたします。</p>
			</div>
		</div>
		</div>
	</section>
