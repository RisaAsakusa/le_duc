<div class="l-content">
	<div class="l-list2">
		<div class="l-list2__left">
			<div class="l-list2__text">
				<div class="c-left">
					（設立）
				</div>
				<div class="c-right">
					昭和23年3月1日
				</div>
			</div>
			<div class="l-list2__text">
				<div class="c-left">
					（組合員）
				</div>
				<div class="c-right">
					正組合員 248名　法人 31名　准組合員 997名
				</div>
			</div>
			<div class="l-list2__text">
				<div class="c-left">
					（役員）
				</div>
				<div class="c-right">
					理事 14名　監事 5名<br>
					代表理事組合長 1名<br>
					専務理事 1名<br>
					常勤監事 1名<br>
					非常勤理事 1名<br>
					非常勤監事 4名<br>
					計 19名<br>
				</div>
			</div>
			<div class="l-list2__text">
				<div class="c-left">
					（設立）
				</div>
				<div class="c-right">
					一般職員 76名（男）・33名（女）計 109名<br>
					工場技術員 17名（男）計 17名<br>
					計 93名（男）・33名（女）<br>
				</div>
			</div>
		</div>
		<div class="l-list2__right">
			<div class="l-list2__text">
				<div class="c-left">
					（出資金）
				</div>
				<div class="c-right">
					総口数 272,924口<br>
					総額 1,264,620千円<br>
					一口金額 5,000円<br>
					最高限度 10,000口<br>
				</div>
			</div>
			<div class="l-list2__text">
				<div class="c-left">
					（運営）
				</div>
				<div class="c-right">
					総会 − 毎年5月開催（通常<br>
					理事会 − 毎月1回（定例）<br>
					監査 − 4半期毎（定例）<br>
				</div>
			</div>
			<div class="l-list2__text">
				<div class="c-left">
					（外かく組織）
				</div>
				<div class="c-right">
					◇ 酪農事業推進部会 − 16名<br>
					◇ 農産事業推進部会 − 16名<br>
					◇ 畜産事業推進部会 − 4名<br>
					◇ 作業受委託事業推進部会 − 16名<br>
					◇ 女性の会 − 36名<br>
					◇ JA青年部 − 83名<br>
					◇ JA女性部 − 141名<br>
					（フレッシュミズ会員54名・喜楽会員50名）<br>
					◇ 熟年会 − 29名<br>
					◇ 年金友の会 − 901名<br>
				</div>
			</div>
		</div>
	</div>
</div>